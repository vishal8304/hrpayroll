﻿using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HRPayroll.Controllers.Accounts
{

    public class LoginController : Controller
    {
        DBEntities Db = new DBEntities();
        //[OutputCache(Duration = 20)]
        public ActionResult Login()
        {

            return View();
        }
        //[OutputCache(Duration=20)]
        [HttpPost]
        public ActionResult Login(LoginDTO model)
        {
            var data = Db.Logins.FirstOrDefault(a => a.Username == model.Username && a.Passwrd == model.Passwrd);
            if (data != null || !ModelState.IsValid)
            {
                FormsAuthentication.SetAuthCookie(data.Username.ToString(), false);               //var lastLoginTime = data.Logintime;
                //data.IsLogin = true;
                data.Logintime = DateTime.Now.ToShortTimeString();
                Db.SaveChanges();
                if (data.Role.RoleName == "Admin")
                {
                    Session["time"] = data.Logintime;
                    Session["name"] = data.Name;
                    Session["RoleId"] = data.RoleId;
                    data.IsLogin = true;
                    Db.SaveChanges();

                    return RedirectToAction("Admin", "Admin");
                }
                else if (data.Role.RoleName == "SemiAdmin" && data.empstatu.Status == "Active")
                {
                    Session["name"] = data.Name;
                    Session["time"] = data.Logintime;
                    data.IsLogin = true;
                    Db.SaveChanges();
                    return RedirectToAction("semi", "semi" );
                }
                else if (data.Role.RoleName == "User" && data.empstatu.Status == "Active")
                {
                    if (data.IsLogin == true)
                    {
                        Session["name"] = data.Name;
                        Session["time"] = data.Logintime;
                        return RedirectToAction("user", "user");
                    }
                    else
                    {
                        return RedirectToAction("Login", "Login");

                    }

                }
                else
                {

                    ViewData["msg"] = "You are not valid user.";
                    return RedirectToAction("Login");
                }
            }
            else
            {
                ViewData["msg"] = "Invalid Username or Password.";

            }
            return View();
        }



        //public ActionResult RedirectToAction()
        //{
        //    string[] roles = Roles.GetRolesForUser();
        //    if (roles.Contains("Admin"))
        //    {
        //        return RedirectToAction("Admin", "Admin");
        //    }
        //    else if (roles.Contains("Semi"))
        //    {
        //        return RedirectToAction("semi", "semi");
        //    }
        //    else if (roles.Contains("User"))
        //    {
        //        return RedirectToAction("Index", "User");
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login");
        //    }

        //}

        public ActionResult Logout()
        {
            Login data = new Login();
            data.IsLogin = false;
            Db.SaveChanges();
            Session.Abandon();
            Session.RemoveAll();
            Session.Clear();
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Login");


        }
    }



}

