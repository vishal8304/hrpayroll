﻿using AutoMapper;
using HRPayroll.Models;
using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Controllers
{
    public class UsersController : Controller
    {
        DBEntities ent = new DBEntities();

        public ActionResult NewUser()
        {
            var model = new UserDTO();
            model.RoleList = new SelectList(ent.Roles.ToList(), "Id", "RoleName");
            model.StatusList = new SelectList(ent.empstatus.ToList(), "Id", "Status");
            return View(model);

        }


        [HttpPost]
        public ActionResult NewUser(UserDTO model)
        {
            try
            {
                model.RoleList = new SelectList(ent.Roles.ToList(), "Id", "RoleName");
                model.StatusList = new SelectList(ent.empstatus.ToList(), "Id", "Status");
                if (ent.Logins.Any(A => A.Username == model.Username))
                {
                    TempData["msg"] = "UserName Already Exist.";
                    return View(model);
                }
                var user = Mapper.Map<Login>(model);
                user.IsDeleted = false;
                ent.Logins.Add(user);
                ent.SaveChanges();
                TempData["msg"] = "Successfully Created a New User.";

            }
            catch (Exception ex)
            {
            
            }
                return RedirectToAction("ShowUsers");

        }

        public ActionResult ShowUsers()
        {
            var users = new UserDTO();
            var userList = (from user in ent.Logins
                            join role in ent.Roles on user.RoleId equals role.Id
                            join status in ent.empstatus on user.StatusId equals status.Id
                            select new UsersVM
                            {
                                Id = user.Id,
                                role = role.RoleName,
                                Name = user.Name,
                                Username = user.Username,
                                Passwrd = user.Passwrd,
                                Status = status.Status
                            }).ToList();
            users.UsersList = userList;
            return View(users);
        }

        public ActionResult ChangePassword(int id)
        {
            var model = new ChangePasswordDTO();
            model.Id = id;
            return View(model);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordDTO model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userData = ent.Logins.Find(model.Id);
            if (userData != null)
            {
                userData.Passwrd = model.password;
                ent.SaveChanges();
                TempData["msg"] = "SuccessFully Updated";

            }
            return RedirectToAction("ChangePassword", new { id = model.Id });
        }

        public ActionResult UserStatus(int id)
        {
            var model = new UserStatusDTO();
            model.Id = id;
            var query = @"select Login.Name, empStatus.Status from Login join empStatus on Login.StatusId = empStatus.Id where Login.Id="+id;
            var data = ent.Database.SqlQuery<users>(query).ToList();
            model.userList = data;
            model.StatusList = new SelectList(ent.empstatus.ToList(), "Id", "Status");
            return View(model);
        }

        [HttpPost]
        public ActionResult UserStatus(UserStatusDTO model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userData = ent.Logins.Find(model.Id);
            if (userData != null)
            {
                userData.StatusId = model.statusId;
                ent.SaveChanges();
                TempData["msg"] = "SuccessFully Updated";

            }
            return RedirectToAction("UserStatus", new { id = model.Id });

        }

        //public ActionResult EditProfile(int id)
        //{
        //    var users = new UserDTO();
        //    users.RoleName = new SelectList(ent.Roles.ToList(), "Id", "RoleName");
        //    var userList = (from user in ent.Logins
        //                    join role in ent.Roles on user.RoleId equals role.Id
        //                    where user.Id==id
        //                    select new profileChange
        //                    {
        //                        Id = user.Id,
        //                        RoleName = role.RoleName,
        //                        Name = user.Name,
        //                        UserName = user.Username,
        //                      }).ToList();
        //    users.UsersChange = userList;
        //    return View(users);
        //}

        //[HttpPost]
        //public ActionResult EditProfile( profileChange users)
        //{
        //    try
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            users.RoleName = new SelectList(a , "Id", "RoleName");
        //            return View(users);
        //        }

        //        var userData = ent.Logins.Find(users.Id);
        //        if(userData != null) {
        //            userData.Name = users.Name;
        //            userData.RoleId = users.RoleId;
        //            userData.Username = users.Username;
        //            ent.SaveChanges();
        //            return RedirectToAction("EditProfile");
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return View(users);

        //}


    }
}
