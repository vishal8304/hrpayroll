﻿using AutoMapper;
using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Controllers
{
    [Authorize(Roles = "User")]
    [OutputCache(Duration = 20)]
    public class UserController : Controller
    {
        DBEntities ent = new DBEntities();
        [OutputCache(Duration = 5)]
        // GET: User
        public ActionResult user()
        {
            return View();
        }
        [OutputCache(Duration = 20)]
        public ActionResult DepartmentSearch(EmployeeListModel model, int pageNumber = 1)
        {
            //model.StatusList = new SelectList(ent.empstatus.ToList(), "Id", "Status");
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            //Linq qUERY
            //            var data = (from r in ent.Employees
            //                        join d in ent.Departments on r.DeptId equals
            //d.DeptId
            //                        join o in ent.Organisations on r.OrgId equals o.OrgId
            //                        select new EmployeeDTO
            //                        {
            //                            Name=r.Name,
            //                            Aadhar_Number=r.Aadhar_Number
            //                        }
            //                        ).ToList();

            string query = @"select emp.*,d.DeptName,org.OrgName from Employee emp join Department d 
           on emp.DeptId=d.DeptId join  Organisation org on emp.OrgId = org.OrgId";
            var data = ent.Database.SqlQuery<EmployeeList>(query).ToList();

            int totalRecord = data.Count;
            int pageSize = 5;
            double totalPages = Math.Ceiling((double)totalRecord / pageSize);
            data = data.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            model.PageNumber = pageNumber;
            model.TotalPages = Convert.ToInt32(totalPages);
            if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                data = data.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();

            model.emplist = data;
            return View(model);
        }
        [OutputCache(Duration = 20)]
        public ActionResult ViewPerson(string term)
        {
            var model = new EmployeeListModel();
            var emp = (from r in ent.Employees
                       join d in ent.Departments on r.DeptId equals
d.DeptId
                       join o in ent.Organisations on r.OrgId equals o.OrgId
                       select new EmployeeList
                       {
                           EmpId = r.EmpId,
                           Name = r.Name,
                           DeptName = d.DeptName,
                           OrgName = o.OrgName
                       }
                                  ).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                emp = emp.Where(a => a.Name.StartsWith(term)).ToList();
            }

            var data = new EmployeeListModel();

            data.emplist = emp;
            return View(data);
        }
        [OutputCache(Duration = 20)]
        public ActionResult ViewAttendece()
        {
            int Month = DateTime.Now.Month;

            // Create List
            List<EmployeeAtt> empWithDate = new List<EmployeeAtt>();

            using (var ent = new DBEntities())
            {
                empWithDate = ent.Attendences.Where(a => a.Date.Value.Month == Month)
                     .Select(a =>
                     new EmployeeAtt
                     {
                         EmpId = a.Id,
                         Date = a.Date,
                         Attendence = a.Atendence.Attendence
                     }).OrderBy(a => a.Date).ToList();

            }

            return View(empWithDate);
        }
     
        public ActionResult GetDepartmentsByOrgId(int OrgnisationId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrgnisationId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}