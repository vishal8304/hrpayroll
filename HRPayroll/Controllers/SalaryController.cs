﻿using AutoMapper;
using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Controllers
{
    public class SalaryController : Controller
    {
        DBEntities ent = new DBEntities();
        // GET: Salary
        public ActionResult salary(string OrgId, string DeptId, string Month)
        {
            var model = new salarylist();
            ViewBag.mth = Month;
            model.DeptList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrgList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            var query = @"SELECT Convert(int,ROW_NUMBER() OVER (ORDER BY EmpId)) AS Serial_No, 
                       emp.EmpId, emp.Name,  org.OrgId as OrgId, dept.DeptId as DeptId,
                       IsNull(SUM(availCL),0) as CL, 
                       IsNull(SUM(Casual_Leave),0) as EncashedLeaves
                       FROM LeavesLedger 
                       join Employee emp on emp.EmpId = LeavesLedger.Emp_Id 
                       join Organisation org on org.OrgId = emp.OrgId
					   join Department dept on dept.DeptId = emp.DeptId
                       where Month(Dated)='" + Month+ "' and org.OrgId='"+OrgId+"' and dept.DeptId='"+DeptId+ "' group by EmpId, Name, org.OrgId, dept.DeptId";
            var data = ent.Database.SqlQuery<BasicDetails>(query).ToList();
            int Mth = Convert.ToInt32(Month);
            var Dinak = model.date;
            var maje = Convert.ToInt32(Dinak);
            var check = ent.Attendences.Any(a => a.att_Id == null && a.Date == null && maje == Mth);
            bool test = true;
            if (string.IsNullOrEmpty(OrgId) && string.IsNullOrEmpty(DeptId) && string.IsNullOrEmpty(Month))
            {

                return View(model);
            }
            else if (check == test)
            {
                var check1 = (!string.IsNullOrEmpty(OrgId) && !string.IsNullOrEmpty(DeptId) && !string.IsNullOrEmpty(Month));
                ViewBag.Msg = "Not";
                ViewBag.clr = "Please Complete the Attendence";
               return View(model);
            }
            else
            {
                ViewBag.Msg = "Yes";
                ViewBag.clr1 = DateTime.Now.AddMonths(-1).ToString("MMMM");
                if (model != null && model.OrgId != null && model.DeptId != null)
                    data = data.Where(a => a.OrgId== model.OrgId && a.DeptId == model.DeptId).ToList();
                model.BasicDetails = data;
                return View(model);

            }

           //return View(model);
        }

        public ActionResult GetDepartmentsByOrgId(int OrgnisationId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrgnisationId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getTp(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.Attendences.Where(a => a.Date.Value.Month == month && a.Emp_Id == EmpId && a.Atendence.Attendence == "Present").Count();
            return PartialView(data);
        }

        public ActionResult getTl(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.Attendences.Where(a => a.Date.Value.Month == month && a.Emp_Id == EmpId && a.Atendence.Attendence == "Absent").Count();
            return PartialView(data);
        }

        public ActionResult getncl(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.NCLs.Where(a => a.dated.Value.Month == month && a.Emp_Id == EmpId).Count();
            return PartialView(data);

        }

        public ActionResult getAdvance(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.empSalaries.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).FirstOrDefault();
            return PartialView(data==null?0:data.Balance_Advance);
        }
        public ActionResult Adjusted(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.empSalaries.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).FirstOrDefault();
            return PartialView(data == null ? 0 : data.Advance_Due);
        }

        public ActionResult getSal(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.SalaryLedgers.Where(a => a.Date.Value.Month == month && a.EmpId == EmpId).FirstOrDefault();
            return PartialView(data == null ? 0 : data.CurrentSalary);
        }

        public ActionResult getCL(string OrgId, string DeptId, int month, int EmpId)
        {
            var data = ent.LeavesLedgers.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).Count();
            return PartialView(data);

        }

        public ActionResult getFL(string OrgId, string DeptId, int month, int EmpId)
        {
            var model = new LeaveAmount();
            model.empAttList = ent.Attendences.ToList();
            model.empsalaryList = ent.SalaryLedgers.ToList();
            model.empLeavesList = ent.LeavesLedgers.ToList();
            model.empNCLList = ent.NCLs.ToList();
            model.TL = ent.Attendences.Where(a => a.Date.Value.Month == month && a.Emp_Id == EmpId && a.att_Id == 2).Count();
            model.CL = ent.LeavesLedgers.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).Select(a => a.availCL).Count();
            model.NCL = ent.NCLs.Where(a => a.dated.Value.Month == month && a.Emp_Id == EmpId).Count();
            ViewBag.msg = (model.TL - model.CL - model.NCL);
            model.FL = ViewBag.msg;
            return PartialView(model == null? 0: model.FL);

        }

        public ActionResult getAmount(string OrgId, string DeptId, int month, int EmpId)
        {
            var model = new LeaveAmount();
            model.empAttList = ent.Attendences.ToList();
            model.empsalaryList = ent.SalaryLedgers.ToList();
            model.empLeavesList = ent.LeavesLedgers.ToList();
            model.empNCLList = ent.NCLs.ToList();
            model.TL = ent.Attendences.Where(a => a.Date.Value.Month == month && a.Emp_Id == EmpId && a.att_Id == 2).Count();
            model.CL = ent.LeavesLedgers.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).Count();
            model.NCL = ent.NCLs.Where(a => a.dated.Value.Month == month && a.Emp_Id == EmpId).Count();
            ViewBag.msg = (model.TL - model.CL - model.NCL);
            model.FL = ViewBag.msg;
            var terminate = ent.SalaryLedgers.Where(a => a.Date.Value.Month == month && a.EmpId == EmpId).FirstOrDefault();
            model.currentSalary = terminate == null ? 0 : terminate.CurrentSalary;
            ViewBag.msg1 = Math.Round((decimal)((model.currentSalary / 30) * model.FL));
            model.amt = ViewBag.msg1;
            return PartialView(model == null ? 0 : model.amt);
        }

        public ActionResult getAmt(string OrgId, string DeptId, int month, int EmpId)
        {
            var model = new LeaveAmount();
            model.empsalaryList = ent.SalaryLedgers.ToList();
            model.empLeavesList = ent.LeavesLedgers.ToList();
            model.EncashedCls = ent.LeavesLedgers.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).Select(a=> a.Casual_Leave).Count();
            var terminate = ent.SalaryLedgers.Where(a => a.Date.Value.Month == month && a.EmpId == EmpId).FirstOrDefault();
            model.currentSalary = terminate == null ? 0 : terminate.CurrentSalary;
            var check = Math.Round((decimal)((model.currentSalary / 30) * model.EncashedCls));
            ViewBag.msg1 = check == null ? 0 : check; 
            model.amt1 = ViewBag.msg1;
            return PartialView(model == null ? 0 : model.amt1);
        }

        public ActionResult getSalary(string OrgId, string DeptId, int month, int EmpId)
        {
            var model = new SalaryReturn();
            model.empsalaryList = ent.SalaryLedgers.ToList(); // Salary Ledger List
            model.empLeavesList = ent.LeavesLedgers.ToList(); //Leaves Ledger List
            model.empSalList = ent.empSalaries.ToList(); //Employee Salary List for Advance Adjusted
            model.empAttList = ent.Attendences.ToList(); //Attendence List 
            model.empNCLList = ent.NCLs.ToList(); //List of NCLs
            //get Leave Amount using Total Leaves and availCl(CASUAL Leaves) and NCL(Non Casual Leaves) FL =(TL-CL-NCL)
            model.TL = ent.Attendences.Where(a => a.Date.Value.Month == month && a.Emp_Id == EmpId && a.att_Id == 2).Count(); // Count the number of Total Leaves
            model.CL = ent.LeavesLedgers.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).Count(); // Count the number of total CLs 
            model.NCL= ent.NCLs.Where(a => a.dated.Value.Month == month && a.Emp_Id == EmpId).Count(); //Count the number of total NCLs
            ViewBag.msg = (model.TL - model.CL - model.NCL); 
            model.FL = ViewBag.msg; //Store the ViewBag Value into the Model.FL(declared in Model Class)
            var terminate = ent.SalaryLedgers.Where(a => a.Date.Value.Month == month && a.EmpId == EmpId).FirstOrDefault();
            model.currentSalary = terminate == null ? 0 : terminate.CurrentSalary;
            ViewBag.msg1 = ((model.currentSalary / 30) * model.FL);
            model.amt = ViewBag.msg1;
            //get Advance Adjusted of each Employee
            model.AdjustedAmount = ent.empSalaries.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).FirstOrDefault().Advance_Due;
            //Get the Encashed Amount using Current Salary and Encashed CLs((Current Salary/30)*EncashedCLs)
            model.EncashedCls = ent.LeavesLedgers.Where(a => a.Dated.Value.Month == month && a.Emp_Id == EmpId).Select(a => a.Casual_Leave).Count();
            ViewBag.msg2 = ((model.currentSalary / 30) * model.EncashedCls);
            model.amt1 = ViewBag.msg2;
            //Total Salary Amount(Current Salary - Leaves Amount - Advance Adjusted + Encashed Amount)
            var calCulate = Math.Round((decimal) (model.currentSalary - model.amt - model.AdjustedAmount + model.amt1));
            ViewBag.msg3 =  calCulate == null ? 0: calCulate;
            model.TotalSalary = ViewBag.msg3;
            return PartialView(model == null ? 0 : model.TotalSalary);
        }

   
        [HttpPost]
        public ActionResult SaveSalary (SalaryMasters array)
        {
            //var model = new SalaryMaster();
            string msg = "";
            if(array.salaryMasterPost.Count <0)
            {
                return View(array);

            }
            else
            {
                try {
                    foreach (var item in array.salaryMasterPost)
                    {
                        var sal = new SalaryMaster
                        {
                            CurrentSalary = item.CurrentSalary,
                            AdjustedAdvance = item.adjustedAmt1,
                            Amount = item.amount,
                            AdvanceBalance = item.AdvanceAmount,
                            CL = item.CL1,
                            Emp_Id = item.Emp_Id,
                            EncashedCls = item.encashed,
                            FL = item.total,
                            LeaveAmount = item.LeaveAmount,
                            NCL = item.ncl,
                            salary = item.salary,
                            TL = item.TL,
                            TP = item.TP,
                            Dated = DateTime.Now,
                            PaidStatus=item.Status,
                            IsPaid = false
                        };
                        ent.SalaryMasters.Add(sal);
                    }
                    ent.SaveChanges();
                    
                }
                catch(Exception ex)
                {
                    msg = "Error";
                }
            }
            TempData["msg"] = msg;
            return Json(msg,JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangeStatus(string OrgId, string DeptId, string Month)
        {

            
            var model = new SalaryMasters();
            ViewBag.mth = Month;
            ViewBag.Status = DateTime.Now.AddMonths(-1).ToString("MMMM");
            TempData["Mth"] = Month;
            TempData["OrgId"] = OrgId;
            TempData["DeptId"] = DeptId;
            model.DeptList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrgList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            var query = @"select SalaryMaster.Id, SalaryMaster.Emp_Id, SalaryMaster.TP, SalaryMaster.TL,SalaryMaster.CL,SalaryMaster.NCL, SalaryMaster.FL, SalaryMaster.LeaveAmount,SalaryMaster.AdvanceBalance as AdvanceAmount,SalaryMaster.AdjustedAdvance as adjustedAmt1, SalaryMaster.EncashedCLs as encashed, SalaryMaster.Amount, SalaryMaster.salary, SalaryMaster.IsPaid, SalaryMaster.CurrentSalary
,Employee.Name, SalaryMaster.PaidStatus as Status from SalaryMaster join Employee on SalaryMaster.Emp_Id = Employee.EmpId
join Organisation org on org.OrgId = Employee.OrgId
					   join Department dept on dept.DeptId = Employee.DeptId
  where Month(Dated)='" + Month+"' and org.OrgId='"+OrgId+"' and dept.DeptId='"+DeptId+"'";
            var data = ent.Database.SqlQuery<SalaryMasterPost>(query).ToList();
            if (string.IsNullOrEmpty(OrgId) && string.IsNullOrEmpty(DeptId) && string.IsNullOrEmpty(Month))
            {

                return View(model);
            }
            else
            {
                if (model != null && model.OrgId != null && model.DeptId != null)
                    data = data.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList();
                model.salaryMasterPost = data;
                return View(model);
            }
            
        }


        public ActionResult printSalarySlip(int Emp_Id)
        {
            var model = new SalaryMasters();
            var data = (from e in ent.Employees
                        join dept in ent.Departments on e.DeptId equals dept.DeptId
                        join Org in ent.Organisations on e.OrgId equals Org.OrgId
                        join sal in ent.SalaryMasters on e.EmpId equals sal.Emp_Id
                        where e.EmpId == Emp_Id
                        select new SalaryMasterPost
                        {
                            Name=e.Name,
                            LeaveAmount=sal.LeaveAmount,
                            amount=sal.Amount,
                            CurrentSalary=sal.CurrentSalary,
                            salary=sal.salary,
                            TP=sal.TP,
                            TL=sal.TL,
                            OrgId=Org.OrgId,
                            DeptId = dept.DeptId,
                            OrgName= Org.OrgName,
                            DeptName=dept.DeptName
                            
                        }).ToList();
            model.salaryMasterPost = data;
            return View(model);
        }


        public ActionResult updateSalary(int Id)
        {
            var Month = TempData["Mth"];
            var OrgId = TempData["OrgId"];
            var DeptId = TempData["DeptId"];
            ent.Database.ExecuteSqlCommand("Update SalaryMaster set IsPaid = case when IsPaid = 0 then 1 when IsPaid = 1 then 0 else 0 end where Emp_Id='"+Id );
            return RedirectToAction("ChangeStatus", new { Month, OrgId, DeptId});
        }

    }
}