﻿using AutoMapper;
using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HRPayroll.Utility;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;

namespace HRPayroll.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        DBEntities ent = new DBEntities();



        public ActionResult Admin()
        {
            //if (!string.IsNullOrWhiteSpace(returnUrl))
            //{
            //    return Redirect(returnUrl);
            //}
            return base.View();
        }


        //    [HttpPost, AllowAnonymous]
        //    public ActionResult Login(Login model)
        //    {
        //        var data = ent.Logins.FirstOrDefault(a => a.Username == model.Username && a.Passwrd == model.Passwrd);
        //        if (data != null || ModelState.IsValid)
        //        {
        //            FormsAuthentication.SetAuthCookie(data.Id.ToString(), false);
        //            TempData["Role"] = data.Role;
        //            TempData["name"] = data.Username;
        //            return RedirectToAction("Home");
        //        }
        //        return View(model);
        //    }

        //    public ActionResult Home()
        //    {

        //        return View();
        //    }


        //    public ActionResult AddEmployee()
        //    {
        //        return View();
        //    }

        //    [HttpPost]
        //    public ActionResult AddEmployee(EmployeeDTO model)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return View(model);
        //        }
        //        TempData["msg"] = "ok";
        //        var emp = Mapper.Map<Employee>(model);
        //        ent.Employees.Add(emp);
        //        return RedirectToAction("AddEmployee");
        //    }

        //}


        //Controls Page
        public ActionResult Controls()
        {
            return base.View();
        }
        //End Of Control Page


        //Create Organisation
        [HttpGet]
        public ActionResult CreateOrganisation()
        {
            var model = new OrganisationDTO();
            return View(model);

        }



        [HttpPost]
        public ActionResult CreateOrganisation(OrganisationDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                var org = Mapper.Map<Organisation>(model);
                org.IsDeleted = false;
                ent.Organisations.Add(org);
                ent.SaveChanges();
                TempData["msg"] = "Successfully Saved";
                return RedirectToAction("CreateOrganisation");
            }
            catch (Exception ex)
            {
                TempData["msg"] = "Server Error";
                return RedirectToAction("CreateOrganisation");

            }
        }
        //END

        //Create Department
        [HttpGet]
        public ActionResult CreateDepartment()
        {
            var model = new DepartmentDTO();
            model.Organizations = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            var list = ent.Organisations.ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateDepartment(DepartmentDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.Organizations = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
                    return View(model);
                }
                var dept = Mapper.Map<Department>(model);
                dept.IsDeleted = false;
                ent.Departments.Add(dept);
                ent.SaveChanges();
                TempData["msg"] = "Successfully Saved";
                return RedirectToAction("CreateDepartment");
            }
            catch (Exception ex)
            {
                TempData["msg"] = "Server Error";
                return RedirectToAction("CreateDepartment");

            }
        }
        //End

        //Create Employee
        [HttpGet]
        public ActionResult CreateEmployee()
        {
            var model = new EmployeeDTO();
            //Taking the list from another table
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateEmployee(EmployeeDTO model)
        {
            try
            {
                var emp = Mapper.Map<Employee>(model);
                if (model.Profileimage != null)
                {
                    string Result = FileOperation.UploadImage(model.Profileimage, "EmployeeImages");
                    model.profilePic = Result;
                }
                emp.DOB = model.DOB;
                emp.Joining_Date = model.Joining_Date;
                emp.IsDeleted = false;
                emp.OrgId = model.OrganisationId;
                emp.DeptId = model.DeptId;
                emp.profilePic = model.profilePic;
                ent.Employees.Add(emp);
                ent.SaveChanges();
                if (emp.EmpId > 0)
                {//Issued Salary at the Time Of Employee Registration
                    var salaryLedger = new SalaryLedger
                    {
                        EmpId = emp.EmpId,
                        CurrentSalary = model.Salry,
                        Date = DateTime.Now,
                        Remarks = "Joining Salary",
                        IsDeleted = false
                    };
                    ent.SalaryLedgers.Add(salaryLedger);
                    ent.SaveChanges();
                }
                if (emp.EmpId > 0)
                {
                    var leavesLeadger = new LeavesLedger
                    {
                        Emp_Id = emp.EmpId,
                        availCL = 0,//Issued a CL at the Time Of Employee Registration
                        Dated = DateTime.Now,
                        Casual_Leave = 0,
                        Balance_CL = 0,
                        Adjusted_Date = DateTime.Now,
                        IsDeleted = false,
                        Remarks = "Issued First CL"
                    };
                    ent.LeavesLedgers.Add(leavesLeadger);
                    ent.SaveChanges();
                }
                if (emp.EmpId > 0)
                {
                    var leavesLeadger = new empSalary
                    {
                        Emp_Id = emp.EmpId,
                        Advance_Due = 0,//Issued a CL at the Time Of Employee Registration
                        Dated = DateTime.Now,
                        Balance_Advance = 0,
                        Issue_Advance = 0,
                        Adjusted_Date = DateTime.Now,
                        IsDeleted = false,
                    };
                    ent.empSalaries.Add(leavesLeadger);
                    ent.SaveChanges();
                }
                if (emp.EmpId > 0)
                {
                    var leavesLeadger = new NCL
                    {
                        Emp_Id = emp.EmpId,
                        NCL1 = 0,//Issued a CL at the Time Of Employee Registration
                        dated = DateTime.Now,
                        NCL_Adjusted = 0,
                        NCL_Balance = 0,
                        Adjusted_Date = DateTime.Now,
                    };
                    ent.NCLs.Add(leavesLeadger);
                    ent.SaveChanges();
                }
                if (emp.EmpId > 0)
                {
                    var StatusMasters = new StatusMaster
                    {
                        Emp_Id = emp.EmpId,
                        StatusId = 1
                    };
                    ent.StatusMasters.Add(StatusMasters);
                    ent.SaveChanges();

                }
                var query = @"select Top 1 Name as EmpName from Employee order by EmpId desc ";
                var data = ent.Database.SqlQuery<NameOfEmployee>(query).ToList();
                model.empName = data;
                TempData["msg"] = "Employee Succssfully Created '" + model.empName.FirstOrDefault().EmpName + "'";
                return RedirectToAction("CreateEmployee");

            }
            catch (Exception ex)
            {
                TempData["msg"] = "Server Error";
                return RedirectToAction("CreateEmployee");
            }


        }
        //End

        //Show Employees with Joining Dates
        public ActionResult ShowEmployees(DateTime? sDate, DateTime? eDate, int? page)
        {

            var data = ent.Employees.ToList();
            if (sDate != null && eDate != null)
            {
                data = data.Where(a => a.Joining_Date >= sDate && a.Joining_Date <= eDate).ToList();
            }
            return View(data);
        }
        //End



        //Department Search
        public ActionResult DepartmentSearch(EmployeeListModel model, string OrganisationId, string DepartmentId)
        {

            try
            {
                model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
                model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
                ViewBag.Org = OrganisationId;
                ViewBag.Dep = DepartmentId;
                var DeptId = ViewBag.Dep;
                var OrgId = ViewBag.Org;
                if (ViewBag.Dep != null)
                {
                    DeptId = ViewBag.Dep;
                    OrgId = ViewBag.Org;
                    model.DepartmentList = new SelectList(ent.Departments.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList(), "DeptId", "DeptName");
                }
                string query = @"select emp.*,d.DeptName,org.OrgName from Employee emp join Department d 
   on emp.DeptId=d.DeptId join  Organisation org on emp.OrgId = org.OrgId";
                var data = ent.Database.SqlQuery<EmployeeList>(query).ToList();
                if (string.IsNullOrEmpty(OrganisationId) && string.IsNullOrEmpty(DepartmentId))
                {
                    ViewBag.Msg = "No Data is Present";
                    return View(model);
                }
                else
                {
                    if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                        data = data.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
                    model.emplist = data;
                    return View(model);
                }
            }
            catch (Exception ex) { }
            return View();
        }

        //End

        //Edit Attendence and Filter

        public ActionResult Test()
        {
            DateTime dt = Convert.ToDateTime(DateTime.Now);
            DateTime dates = dt.Date;
            var dat = dates.ToShortDateString();
            return View();
        }
        public ActionResult EditAttendence(EditAttendence model, string term, string OrganisationId, string DepartmentId)
        {
            model.AttendenceList = new SelectList(ent.Atendences.Where(a => a.OrgId == model.OrganisationId && a.DepId == model.DepartmentId).ToList(), "Id", "Attendence");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            ViewBag.Org = OrganisationId;
            ViewBag.Dep = DepartmentId;
            TempData["Org"] = OrganisationId;
            TempData["Dep"] = DepartmentId;
            var DeptId = ViewBag.Dep;
            var OrgId = ViewBag.Org;
            if (ViewBag.Dep != null)
            {
                DeptId = ViewBag.Dep;
                OrgId = ViewBag.Org;
                model.DepartmentList = new SelectList(ent.Departments.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList(), "DeptId", "DeptName");
            }
            var Date = TempData["Date"];
            if (string.IsNullOrEmpty(OrganisationId) && string.IsNullOrEmpty(DepartmentId))
            {

            }
            else
            {
                if (model != null && model.OrganisationId != null && model.DepartmentId != null || Date != null)
                {
                    string sqlQuery = "select Employee.EmpId as Id,Employee.Name,Employee.DeptId, Employee.OrgId, Department.DeptName from Employee join Department on Employee.DeptId = Department.DeptId";
                    var emp = ent.Database.SqlQuery<EmployeeList>(sqlQuery).ToList();
                    emp = emp.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
                    model.empList = emp;

                    if (Date != null)
                    {
                        DateTime dates = Convert.ToDateTime(Date);
                        var dat = dates.ToShortDateString();
                        ViewBag.Msg = "Last Updated Attendence on " +dat;
                        string sqlQuery1 = "select emp.Name, att.att_Id as Att_Id, Attendences.Attendence as AttendenceName from employee emp join Attendence att on emp.EmpId = att.Emp_Id join Atendence Attendences on Attendences.Id = att.att_Id where Date='" + Date + "'";
                        var emp1 = ent.Database.SqlQuery<ViewList>(sqlQuery1).ToList();
                        model.view = emp1;
                        model.date = DateTime.Now.Date;
                    }
                }
            }
            return View(model);
        }

        public ActionResult View (string OrgnisationId, string date, string OrganisationId, string DepartmentId)
        {
            
            var model = new EditAttendence();
            DateTime dates = Convert.ToDateTime(date);
            var dat = dates.ToShortDateString();
            ViewBag.Msg = "Attendence as per on " +dat;
            string data = @"select emp.Name, att.att_Id as Att_Id, Atendence.Attendence as AttendenceName from Employee emp join Attendence att on emp.EmpId = att.Emp_Id join Atendence  on Atendence.Id = att.att_Id where Date = " + date + " and emp.OrgId =" + OrganisationId + " and emp.DeptId =" + DepartmentId;
            var emp2 = ent.Database.SqlQuery<ViewList>(data).ToList();
           
            return View(model);
        }
     

        public ActionResult SaveAll()
        {
            List<Attendence> AttInfo = new List<Attendence> { new Attendence { Emp_Id = 0, att_Id = 0 } };
            return View(AttInfo);
        }

        

        public ActionResult SaveCLs(LeavesRetrunModel model)
        {

            var OrgId = model.empList.FirstOrDefault().OrgId;
            var DeptId = model.empList.FirstOrDefault().DeptId;
            string msg = "";
            try
            {
                foreach (var item in model.empList) //Get the List in Emp[i] form
                {
                    var att = new LeavesLedger
                    {
                        Casual_Leave = item.Casual_Leave,
                        Emp_Id = item.EmpId,
                        Adjusted_Date = item.Adjusted_Date,
                        IsDeleted = false
                    };
                    ent.LeavesLedgers.Add(att);
                    ent.SaveChanges();
                    msg = "Record Saved Successfully";
                    }
            }
            catch (Exception ex)
            {
                msg = "error";
            }
            TempData["msg"] = msg;
            return RedirectToAction("CasualLeave", new { OrgId, DeptId });
        }


        [HttpPost]
        public ActionResult SaveAll(List<Attendence> AttInfo)
        {
            if (ModelState.IsValid)
            {
                using (DBEntities ent = new DBEntities())
                {
                    foreach (var i in AttInfo)
                    {
                        ent.Attendences.Add(i);
                    }
                    ent.SaveChanges();
                    ViewBag.Message = "Data successfully saved!";
                    ModelState.Clear();
                    AttInfo = new List<Attendence> { new Attendence { Emp_Id = 0, att_Id = 0 } };
                }
            }
            return View(AttInfo);
        }


        public ActionResult SaveAttendance(EditAttendence model)
        {
            var OrganisationId = model.empList.FirstOrDefault().OrgId;
            var DepartmentId = model.empList.FirstOrDefault().DeptId;
            var dt = model.date;
            string msg = "";
            try
            {
                foreach (var item in model.empList)
                {
                    var att = new Attendence
                    {
                        att_Id = item.Att_ID,
                        Emp_Id = item.Id,
                        Date = model.date,
                        IsDeleted = false
                    };
                    TempData["Date"] = model.date;
                    ent.Attendences.Add(att);
                    if (ent.Attendences.Any(a=> a.att_Id ==att.att_Id && a.Date == att.Date && a.Emp_Id == att.Emp_Id))
                    {
                        msg = "Attendence Already Saved.";
                    }
                    else
                    {
                        ent.SaveChanges();
                        msg = "Attendence Saved Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
            }
            TempData["msg"] = msg;
            return RedirectToAction("EditAttendence", new {OrganisationId, DepartmentId });
        }



        public ActionResult ChangeSalary(string term, ChangeSalaryDTO model)
        {
            //var model = new ChangeSalaryDTO();
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            string query = @"with EmpSal as (select EmpId,CurrentSalary,ROW_NUMBER() over(partition by empid order by currentsalary desc) as EmpRank from SalaryLedger)
select empSal.EmpId,emp.Name,org.OrgId as OrganisationId,empSal.CurrentSalary as New_Salary, ORG.OrgName, dep.DeptId as DeparmenttId, dep.DeptName from EmpSal empSal
join Employee emp on empsal.EmpId = emp.EmpId JOIN Organisation org on org.OrgId = emp.OrgId join Department dep on dep.DeptId = emp.DeptId
 where EmpRank=1";
            var emp = ent.Database.SqlQuery<Salary>(query).ToList();
            if (!string.IsNullOrEmpty(term))
            {
                emp = emp.Where(a => a.Name.StartsWith(term)).ToList();
            }
            if (model != null && model.OrgId != null && model.DeptId != null)
                emp = emp.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList();
            model.salaryUpdate = emp;
            return View(model);
        }

        //public ActionResult ChangeSalary(ChangeSalaryDTO model)
        //{


        //    var data = new ChangeSalaryDTO();
        //    // data.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
        //    var emp = (from p in ent.SalaryLedgers
        //               join c in ent.Employees on p.EmpId equals c.EmpId
        //               select new Salary
        //               {
        //                   EmpId = c.EmpId,
        //                   Name = c.Name,
        //                   Curr_Salary = p.CurrentSalary
        //               }).ToList();


        //    data.salaryUpdate = emp;
        //    return View(data);
        //}

        //Cascading DropDowns
        //public ActionResult cascadingDepartments(cascadingDepartments model)
        //{

        //    List<SelectListItem> Organisations = new List<SelectListItem>();
        //    cascadingDepartments OrgModel = new cascadingDepartments();

        //    List<Organisation> org = ent.Organisations.ToList();
        //    org.ForEach(x =>
        //    {
        //        Organisations.Add(new SelectListItem { Text = x.OrgName, Value = x.OrgId.ToString() });
        //    });
        //    OrgModel.Organisations = Organisations;

        //    return View(OrgModel);
        //}

        public ActionResult GetDepartmentsByOrgId(int OrgnisationId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrgnisationId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDepartmentsByOrg(int orgId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == orgId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDepartmentsByOrg__Id(int OrgnisationId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrgnisationId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDepartment(int OrgId)
        {
            var dpt = ent.Departments.Where(A => A.OrgId == OrgId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDepartments(int OrgId)
        {
            var dpt = ent.Departments.Where(A => A.OrgId == OrgId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOrgnisation()
        {
            var dpt = ent.Organisations.ToList();
            var data = Mapper.Map<IEnumerable<OrganisationDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }



        //public JsonResult getName(string Prefix)
        //{
        //    List<ChangeSalaryDTO> name = new List<ChangeSalaryDTO>();

        //    var data= (from N in name
        //               where N.emp_Name.StartsWith(Prefix)
        //               select new { N.emp_Name, N.EmpId });
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}
        //[HttpPost]
        //public ActionResult ChangeSalary(ChangeSalaryDTO model)
        //{
        //    try
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return View(model);
        //        }

        //        var sal = Mapper.Map<ChangeSalary>(model);
        //        sal.IsDeleted = false;
        //        ent.ChangeSalaries.Add(sal);
        //        ent.SaveChanges();
        //        TempData["msg"] = "Successfully Saved";
        //        return RedirectToAction("ChangeSalary");
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["msg"] = "Server Error";
        //        return RedirectToAction("ChangeSalary");
        //    }

        //}
        //<!--End -->
        public ActionResult GetDepartmentsOrgId(int OrganisationId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrganisationId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Status(StatusDTO model)
        {
            //Update Status of an Employee
            model.StatusList = new SelectList(ent.empstatus.ToList(), "Id", "Status");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            var data = new StatusDTO();
            var emp = (from r in ent.Employees
                       join d in ent.Departments on r.DeptId equals d.DeptId
                       join o in ent.Organisations on r.OrgId equals o.OrgId
                       select new Status
                       {
                           Id = r.EmpId,
                           Name = r.Name,
                           DeptName = d.DeptName,
                           OrgName = o.OrgName,
                           DeptId = d.DeptId,
                           OrgId = o.OrgId
                       }).ToList();

            if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                emp = emp.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
            model.empList = emp;
            return View(model);
        }

        public ActionResult SaveStatus(EmployeeProfileModel model)
        {
            string msg = "";
            try
            {
                var record = ent.StatusMasters.FirstOrDefault(a=> a.Emp_Id == model.Emp_Id);
                if (record == null)
                {
                    return View(model);
                }
                else
                {
                    record.StatusId = model.StatusId;
                }
                ent.SaveChanges();
                msg = "ok";
            }
            catch (Exception ex)
            {
                msg = "error";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        //View Salary of selected Organisation and Departments of an Employee
        //public ActionResult ViewSalary(ViewSalary model)
        //{

        //    var data = new ViewSalary();
        //    var view = (
        //        from e in ent.Employees
        //                      join d in ent.Departments on e.DeptId equals d.DeptId
        //                      join o in ent.Organisations on e.OrgId equals o.OrgId
        //                      join s in ent.SalaryLedgers on e.EmpId equals s.EmpId
        //                      select new salaryList
        //                      {
        //                          Id = e.EmpId,
        //                          Name = e.Name,
        //                          Curr_Salary = s.CurrentSalary,
        //                          DeptName = d.DeptName,
        //                          OrgName = o.OrgName
        //                      }
        //    ).ToList();

        //    if (model != null && model.OrganisationId != null && model.DepartmentId != null)
        //        view = view.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
        //    model.viewsalary = view;
        //    model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
        //    model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");

        //    model.viewsalary = view;
        //    return View(model);
        //}


        //Show and Serach By Name The Person Details
        public ActionResult ViewPerson(string term, EmployeeListModel model, string OrganisationId, string DepartmentId)
        {

            //var model = new EmployeeListModel();
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            ViewBag.Org = OrganisationId;
            ViewBag.Dep = DepartmentId;
            var DeptId = ViewBag.Dep;
            var OrgId = ViewBag.Org;
            if (ViewBag.Dep != null)
            {
                DeptId = ViewBag.Dep;
                OrgId = ViewBag.Org;
                model.DepartmentList = new SelectList(ent.Departments.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList(), "DeptId", "DeptName");
            }
            var emp = (from r in ent.Employees
                       join d in ent.Departments on r.DeptId equals
d.DeptId
                       join o in ent.Organisations on r.OrgId equals o.OrgId
                       select new EmployeeList
                       {
                           EmpId = r.EmpId,
                           Name = r.Name,
                           DeptName = d.DeptName,
                           OrgName = o.OrgName,
                           OrgId = o.OrgId,
                           DeptId = d.DeptId
                       }).ToList();
            //var data = new EmployeeListModel();
            if (string.IsNullOrEmpty(OrganisationId) && string.IsNullOrEmpty(DepartmentId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(model);

            }
            else
            {

                if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                    emp = emp.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
                if (!string.IsNullOrEmpty(term))
                {
                    term = term.ToLower();
                    emp = emp.Where(a => a.Name.ToLower().Contains(term)).ToList();
                }
                model.emplist = emp;
                return View(model);
            }




            //model.emplist = emp;
            // return View(model);
        }


        //public ActionResult ViewAttendece(DateTime? month)
        //{

        //    var attendences = attendences.Where(a => a.Date.Value.Month == month.Value.Month || month.Value.Month == null || month.Value.Month == "").OrderBy(a => a.Date).ToList();
        //    var count = attendences.Count;
        //    for(var i=0;i <count; i=i + 2)
        //    {
        //        if (attendences[i].Date.Value.Date == attendences[i + 1].Date.Value.Date && attendences[i].EmpId == attendences[i + 1].EmpId)
        //        {
        //            if (attendences[i].Date.Value.TimeOfDay < attendences[i + 1].Date.Value.TimeOfDay)
        //            {
        //                var outTime = attendences[i + 1].Date.Value.TimeOfDay;
        //                var inTime = attendences[i].Date.Value.TimeOfDay;
        //                attendences[i].emp_inTime = inTime;
        //                attendences[i].emp_outTime = outTime;

        //            }
        //        }


        //    }

        //    return View(attendences);
        //}


        //Salary View 
        public ActionResult UpdateSalary(int EmpId)
        {
            var model = new SalaryLedgerDTO();
            model.EmpId = EmpId;
            return View(model);
        }


        //Salary Update
        [HttpPost]
        public ActionResult UpdateSalary(SalaryLedgerDTO model)
        {

            var sal = Mapper.Map<SalaryLedger>(model);
            sal.IsDeleted = false;
            sal.Date = DateTime.Now;
            ent.SalaryLedgers.Add(sal);
            ent.SaveChanges();
            TempData["msg"] = "Successfully Updated";
            return RedirectToAction("ViewProfile", new { id = model.EmpId });
        }

        public ActionResult ViewStatus(string term, string OrganisationId, string DepartmentId)
        {
            var data = new StatusDTO();
            string query = @"select emp.EmpId as Id, emp.Name, stat.StatusId, emp_status.Status as empstatus from Employee emp 
Join StatusMaster stat on emp.EmpId = stat.Emp_Id 
join empstatus emp_status  on  emp_status.Id = stat.StatusId ";
            var data1 = ent.Database.SqlQuery<Status>(query).ToList();

            if (string.IsNullOrEmpty(OrganisationId) && string.IsNullOrEmpty(DepartmentId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(data);

            }
            else
            {

                if (data != null && data.OrganisationId != null && data.DepartmentId != null)
                    data1 = data1.Where(a => a.OrgId == data.OrganisationId && a.DeptId == data.DepartmentId).ToList();
                data.empList = data1;
                return View(data);
            }
            if (!string.IsNullOrEmpty(term))
            {
                data1 = data1.Where(a => a.Name.StartsWith(term)).ToList();
            }

            data.empList = data1;

            return View(data);
        }


        // View Attendence of Employees
        public ActionResult ViewAttendece(string term, AttendenceDTO model, string OrgId, string DeptId,string Month1)
         {
            //int Month = DateTime.Now.Month;
            int Mth = Convert.ToInt32(Month1);
            ViewBag.month = Month1;
            ViewBag.OrgId = OrgId;
            ViewBag.DeptId = DeptId;

            TempData["month"] = Month1;
            TempData["OrgId"] = OrgId;
            TempData["DeptId"] = DeptId;

            //model.DepartmentsList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            // Create List
            List<EmployeeAtt> empWithDate = new List<EmployeeAtt>();
            using (var ent = new DBEntities())
            {
                empWithDate = ent.Attendences.Where(a => a.Date.Value.Month == Mth)
                 .Select(a =>
                 new EmployeeAtt
                 {
                     EmpId = a.Emp_Id,
                     Date = a.Date,
                     Attendence = a.Atendence.Attendence,
                     Emp_Name = a.Employee.Name,
                     DeptId = a.Employee.DeptId,
                     OrgId = a.Employee.OrgId,
                     IsFinalised= a.IsFinalised
                 }).OrderBy(a => a.Emp_Name).ToList();
                if (string.IsNullOrEmpty(OrgId) && string.IsNullOrEmpty(DeptId))
                {
                    ViewBag.Msg = "No Data is Present";
                    return View(empWithDate);
                }
                else
                {
                    if (model != null && model.OrgId != null && model.DeptId != null)
                    empWithDate = empWithDate.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList();
                    model.empAtt = empWithDate;
                    if(model.empAtt.FirstOrDefault().IsFinalised == true) {
                        TempData["msg"] = "Attendence Finalise";
                    }
                    else
                    {
                        TempData["msg"] = "Attendence Not Finalise";
                    }
                    return View(empWithDate);
                }
            }

            if (!string.IsNullOrEmpty(term))
            {
                empWithDate = empWithDate.Where(a => a.Emp_Name.StartsWith(term)).ToList();
                model.empAtt = empWithDate;
                if (model.empAtt.FirstOrDefault().IsFinalised == true)
                {
                    TempData["msg"] = "Attendence Finalise";
                }
                else
                {
                    TempData["msg"] = "Attendence Not Finalise";
                }
                return View(empWithDate);
            }
        }

        public ActionResult GetTotalAttendence(int empId, int month)
        {
            var data = ent.Attendences.Where(a => a.Date.Value.Month == month && a.Emp_Id == empId && a.Atendence.Attendence == "Present").Count();
            return PartialView(data);
        }

        public ActionResult ViewAttandence(string date, string OrganisationId, string DepartmentId)
        { 

          
            ViewBag.Msg = "Your Result as per on " +date;
            var model = new EditAttendence();
            string data = @"select emp.Name, att.att_Id as Att_Id, Atendence.Attendence as AttendenceName from Employee emp join Attendence att on emp.EmpId = att.Emp_Id join Atendence  on Atendence.Id = att.att_Id where Date = " + date + " and emp.OrgId =" + OrganisationId + " and emp.DeptId =" + DepartmentId;
            var emp2 = ent.Database.SqlQuery<ViewList>(data).ToList();
            if (emp2.Count() == 0)
            {
                TempData["msg"] = "No Records";
            }
            else
            {
                model.view = emp2;
            }
            return View(model);
        }

        //View Profile Of an Employee
        public ActionResult ViewProfile(int id)
        {
            var emp = ent.Employees.Find(id);
            var salaryLedger = ent.SalaryLedgers.Where(a => a.EmpId == id).OrderByDescending(a => a.Id).ToList(); ;
            var leavesleader = ent.LeavesLedgers.Where(a => a.Emp_Id == id);
            var empSalary = ent.empSalaries.Where(a => a.Emp_Id == id);
            var ncl = ent.NCLs.Where(a => a.Emp_Id == id);
            var Status = ent.StatusMasters.FirstOrDefault(a=>a.Emp_Id == id);
            var model = new EmployeeProfileModel();
            model.Employee = emp;
            model.SalaryLedger = salaryLedger;
            model.leaveslist = leavesleader;
            model.empSalaryList = empSalary;
            if (Status != null)
            {
                model.Status = Status.empstatu.Status;
                model.StatusId = Status.StatusId;
            }
            model.nclList = ncl;
            //var data = ent.SalaryLedgers.Find(id);
            //if(data != null)
            //{
            //    model.CurrentSalary = data.CurrentSalary;
            //    ViewBag.sal = model.CurrentSalary;
            //}



            model.StatusList = new SelectList(ent.empstatus.ToList(), "Id", "Status");
            //model.empSalaryList = salList;
            //var data = (from emp in ent.Employees
            //            where emp.EmpId == id
            //            select new empdetails
            //            {
            //                Name = emp.Name,
            //                DOB = emp.DOB,

            //            }).ToList();

            //    var sal = (from sal1 in ent.SalaryLedgers
            //               where sal1.EmpId == id
            //               select new salDetails
            //               {
            //                   CurrentSalary = (double)sal1.CurrentSalary,
            //                   Date = sal1.Date,
            //               }).ToList();


            //var model = new EmployeeDTO { EmpListDetails = data,SalListDetails =sal };
            //int pageSize = 5;
            //double totalPages = Math.Ceiling((double)totalRecord / pageSize);
            //data = data.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            //model.PageNumber = pageNumber;
            //model.TotalPages = Convert.ToInt32(totalPages);

            //var advance = Convert.ToDouble(empSalary.Max(a => a.Issue_Advance));
            //var sal = Convert.ToDouble(empSalary.Max(a => a.Advance_Due));
            //var model = new LeavesRetrunModel();
            var query = @"select Emp_Id,IsNull(Casual_Leave,0) as Casual_Leave,IsDeleted,IsNull(availCL,0) as availCL,Dated,Remarks,Balance_CL,Adjusted_Date from LeavesLedger where Emp_Id=" + id;
            var data = ent.Database.SqlQuery<empLeave>(query).ToList();
            model.empList = data;

            var query1 = @"select Emp_Id, IsNull(Issue_Advance,0) as Issue_Advance, Adjusted_Date, IsDeleted, IsNull(Advance_Due,0) as Advance_Due, Dated, Remarks, IsNull(Balance_Advance,0) as Balance_Advance from empSalary where Emp_Id=" + id;
            var data1 = ent.Database.SqlQuery<AdvanceIssue>(query1).ToList();
            model.IssueAdv = data1;
            var query2 = @"select Emp_Id,IsNull(NCL,0) as NCL,dated, IsNull(NCL_Adjusted,0) as NCL_Adjusted, IsNull(NCL_Balance,0) as NCL_Balance , Adjusted_Date, Remarks from NCL where Emp_Id=" + id;
            var data2 = ent.Database.SqlQuery<NCLrm>(query2).ToList();
            model.list = data2;
            return View(model);
        }

        [HttpPost]
        public ActionResult StatusBar(StatusDTO model)
        {

            var data = Mapper.Map<StatusMaster>(model);
            ent.Entry(data).State = System.Data.Entity.EntityState.Modified;
            ent.SaveChanges();
            return View();
        }

        //[HttpPost]
        //public ActionResult ChangeStatus (StatusDTO model)
        //{

        //    var status = Mapper.Map<StatusMaster>(model);
        //    ent.StatusMasters.Add();
        //    ent.SaveChanges();
        //    return View(model);
        //}

        //Edit the profile an Employee
        [HttpPost]
        public ActionResult ViewProfile(int id, string propertyName, string value)
        {


            var status = false;
            var message = "";
            var emp = ent.Employees.Find(id);
            if (emp != null)
            {
                ent.Entry(emp).Property(propertyName).CurrentValue = value;
                ent.SaveChanges();
                status = true;
            }
            else
            {
                message = "Error!";
            }
            var response = new { value = value, status = status, message = message };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }

        public ActionResult viewLoginTime()
        {
            var model = new UserDTO();
            var query = @"select Login.Username,Login.Name, Login.Logintime, Login.StatusId, role.RoleName as role from Login join Roles role on Login.RoleId = role.Id where [role].RoleName = 'User'";
            //(from login in ent.Logins
            //            join role in ent.Roles on login.RoleId equals role.Id
            //            where login.Role = role.
            //            select new UsersVM
            //            {
            //                Username = login.Username,
            //                Logintime = login.Logintime,
            //                Status = login.Status,
            //                role = role.RoleName
            //            }).ToList();
            var data = ent.Database.SqlQuery<UsersVM>(query).ToList();
            model.UsersList = data;
            return View(model);
        }


        public ActionResult CasualLeave(LeavesRetrunModel model, string OrgId, string DeptId)
         {
            model.Departments = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.Organisation = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            ViewBag.Org = OrgId;
            ViewBag.Dep = DeptId;
            var Dep = ViewBag.Dep;
            if (ViewBag.Dep != null)
            {
                DeptId = ViewBag.Dep;
                OrgId = ViewBag.Org;
                model.Departments = new SelectList(ent.Departments.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList(), "DeptId", "DeptName");
            }
            
            var query = @"select emp.EmpId,emp.Name,LeavesLedger.Adjusted_Date, org.OrgId, LeavesLedger.Balance_CL, org.OrgName,dep.DeptId, dep.DeptName from  Employee emp  JOIN Organisation org on org.OrgId = emp.OrgId join Department dep on dep.DeptId = emp.DeptId join LeavesLedger on LeavesLedger.Emp_Id = emp.EmpId  where datepart(mm,LeavesLedger.Dated)  =  MONTH(GETDATE()) and LeavesLedger.Balance_CL IS NOT NULL group by LeavesLedger.Adjusted_Date, emp.EmpId,emp.Name, org.OrgId, LeavesLedger.Balance_CL, org.OrgName,dep.DeptId, dep.DeptName";
            var data = ent.Database.SqlQuery<empLeave>(query).ToList();
            if (string.IsNullOrEmpty(OrgId) && string.IsNullOrEmpty(DeptId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(model);

            }
            else
            {

                if (model != null && model.OrgId != null && model.DeptId != null)
                    data = data.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList();
                model.empList = data;
                return View(model);
            }
            
        }
        public ActionResult getDepartmentsDD(int OrgaId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrgaId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MarkCl(int Empid)
        {
            var model = new LeavesRetrunModel();
            var query = @"select Id, Emp_Id,IsNull(Casual_Leave,0) as Casual_Leave,IsDeleted,IsNull(availCL,0) as availCL,Dated,Remarks,Balance_CL,Adjusted_Date from LeavesLedger where Emp_Id=" + Empid;
            var data = ent.Database.SqlQuery<empLeave>(query).ToList();
            model.empList = data;
            //ViewBag.carry = model.empList.Sum(a => a.availCL);
            return View(model);
        }


        [HttpPost]
        public ActionResult MarkCl(LeavesRetrunModel model)
        {

          
                var mark = Mapper.Map<LeavesLedger>(model);
            var record = ent.LeavesLedgers.Find(model.EmpId);
                mark.Emp_Id = model.EmpId;
                mark.Casual_Leave = model.CasualLeaves;
                //mark.IsDeleted = false;
                //mark.Remarks = "Adjusted";
                //mark.availCL = 0;
                //mark.Dated = DateTime.Now;
                ent.LeavesLedgers.Add(mark);
                ent.SaveChanges();
                return RedirectToAction("ViewProfile", new { id = mark.Emp_Id });
            
        }


        public ActionResult GenerateSalry(EmployeeListModel sal)
        {
            //var sal = new generateSalaryRM();
            sal.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            sal.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            string query = @"select emp.*,d.DeptName,org.OrgName from Employee emp join Department d 
            on emp.DeptId=d.DeptId join  Organisation org on emp.OrgId = org.OrgId";
            var data1 = ent.Database.SqlQuery<EmployeeList>(query).ToList();
            if (sal != null && sal.OrganisationId != null && sal.DepartmentId != null)
                data1 = data1.Where(a => a.OrgId == sal.OrganisationId && a.DeptId == sal.DepartmentId).ToList();
            sal.emplist = data1;
            return View(sal);
        }

        public ActionResult Generate(int id)
        {
            var model = new SalaryEmployee();
            string query = @"SELECT DATENAME(MONTH, DATEADD(M, MONTH([Date]), - 1)) Month, 
   d.EmpId, d.Name, a.CurrentSalary as Curr_Salary, b.Issue_Advance as AdvanceGiven, c.Casual_Leave as EncashLeaves, c.availCL
   FROM SalaryLedger a left join empSalary b on a.empSalId = b.Id left join LeavesLedger c on c.Id = a.leaveId join Employee d on a.EmpId = d.EmpId where d.EmpId=" + id + " GROUP BY MONTH([Date]), CurrentSalary, Issue_Advance, Casual_Leave, availCL, d.EmpId, Name HAVING MONTH([Date]) < (SELECT DATEPART(M, DATEADD(M, 0, GETDATE())))  ";
            var data = ent.Database.SqlQuery<SalaryEmployee>(query).FirstOrDefault();

            return View(data);

        }
        [HttpPost]
        public ActionResult Generate()
        {
            var model = new generateSalaryRM();
            double CS = Convert.ToDouble(model.CurrentMonthSalary);
            double Advance = Convert.ToDouble(model.AdvanceGiven);
            int availcls = Convert.ToInt32(model.Leaves);
            int EncashCls = Convert.ToInt32(model.EncashLeaves);
            ViewBag.Salary = (CS) + Advance - ((CS / 30) * availcls) + ((CS / 30) * EncashCls);
            return View(model);
        }



        //Advance Issue to Employee
        public ActionResult IssueAdvance(string OrganisationId, string DepartmentId, IssueAdvance model)
        {

            //var data= new IssueAdvance();
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            var query = @"select emp.EmpId as Emp_Id,emp.Name, org.OrgId, org.OrgName,dep.DeptId, dep.DeptName
  from  Employee emp  JOIN Organisation org on org.OrgId = emp.OrgId join Department dep on dep.DeptId = emp.DeptId";
            var data = ent.Database.SqlQuery<AdvanceIssue>(query).ToList();
            if (string.IsNullOrEmpty(OrganisationId) && string.IsNullOrEmpty(DepartmentId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(model);

            }
            else
            {
                if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                    data = data.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
                model.IssueAdv = data;
                return View(model);
            }
            //return View();
        }


        //Mark Advance of an Employee
        public ActionResult markAdvance(int Empid)
        {
            var model = new IssueAdvance();
            var query = @"select Emp_Id, IsNull(Issue_Advance,0) as Issue_Advance, IsDeleted, IsNull(Advance_Due,0) as Advance_Due, Dated, Remarks,Adjusted_Date,  IsNull(Balance_Advance,0) as Balance_Advance from empSalary where Emp_Id=" + Empid;
            var data = ent.Database.SqlQuery<AdvanceIssue>(query).ToList();
            model.IssueAdv = data;
            return View(model);
        }


        [HttpPost]
        public ActionResult markAdvance(IssueAdvance model)
        {
            var mark = Mapper.Map<empSalary>(model);
            //mark.Dated = DateTime.Now;
            mark.Adjusted_Date = model.AdjustedDate;
            mark.IsDeleted = false;
            mark.Emp_Id = model.EmpId;
            mark.Balance_Advance = model.Balance;
            ent.empSalaries.Add(mark);
            ent.SaveChanges();
            TempData["msg"] = "Advance has been Allotted";
            return RedirectToAction("ViewProfile", new { id = model.EmpId });
        }

        public ActionResult empProfile(EmployeeListModel model)
        {
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            string query = @"select emp.*,d.DeptName,org.OrgName from Employee emp join Department d 
            on emp.DeptId=d.DeptId join  Organisation org on emp.OrgId = org.OrgId";
            var data = ent.Database.SqlQuery<EmployeeList>(query).ToList();
            if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                data = data.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();

            model.emplist = data;
            return View(model);
        }

        public ActionResult generateCls(LeavesRetrunModel model)
        {
            string query = @"select emp.*,d.DeptName,org.OrgName from Employee emp join Department d 
            on emp.DeptId=d.DeptId join  Organisation org on emp.OrgId = org.OrgId";
            var data = ent.Database.SqlQuery<empLeave>(query).ToList();
            model.empList = data;
            return View(model);
        }


        public ActionResult generateCL(int EmpId)
        {
            var model = new LeavesRetrunModel();
            var query = @"select emp.EmpId, emp.Name as Name, leaves.Id, leaves.Adjusted_Date as Adjusted_Date, IsNull(leaves.Balance_CL,0) as Balance_CL from Employee emp join LeavesLedger leaves on emp.EmpId= leaves.Emp_Id WHERE DATEPART(m, Adjusted_Date) = DATEPART(m, DATEADD(m, -1, getdate())) AND DATEPART(yyyy, Adjusted_Date) = DATEPART(yyyy, DATEADD(m, -1, getdate())) and EMP_Id="+EmpId;
            var data = ent.Database.SqlQuery<empLeave>(query).ToList();
            model.empList = data;
          
            return View(model);
        }

        [HttpPost]
        public ActionResult generateCL(LeavesRetrunModel model)
        {
            var data = new LeavesLedger();
           
            // data.Dated = DateTime.Now;
            //data.Casual_Leave = 0;
            //var record = ent.LeavesLedgers.Find(model.Id);
            //if (record != null)
            //{
                data.Dated = model.Dated;
                data.Emp_Id = model.EmpId;
                data.availCL = model.availCL; //Get the Issued CLs of Month
                data.Remarks = model.Remarks;//Get the Remarks
                ent.LeavesLedgers.Add(data);
                
            
            ent.SaveChanges();
            TempData["msg"] = "CL has been Allotted";
            return RedirectToAction("ViewProfile", new { id = model.EmpId });
        }


        [HttpGet]
        public ActionResult generateAD(int EmpId)
        {
            var model = new IssueAdvance();
            var query = @"select emp.EmpId as Emp_Id, emp.Name, leaves.Adjusted_Date as Adjusted_Date, IsNull(leaves.Balance_Advance,0) as Balance_Advance from Employee emp join empSalary leaves on emp.EmpId= leaves.Emp_Id WHERE DATEPART(m, Adjusted_Date) = DATEPART(m, DATEADD(m, -1, getdate())) AND DATEPART(yyyy, Adjusted_Date) = DATEPART(yyyy, DATEADD(m, -1, getdate())) and emp.EmpId=" + EmpId;
            var data = ent.Database.SqlQuery<AdvanceIssue>(query).ToList();
            model.IssueAdv = data;
            return View(model);
        }

        [HttpPost]
        public ActionResult generateAD(IssueAdvance model)
        {
            var mark = Mapper.Map<empSalary>(model);
            //mark.Dated = DateTime.Now;
            mark.Dated = model.Dated;
            mark.Advance_Due = 0;
            mark.Emp_Id = model.EmpId;
            mark.IsDeleted = false;
            mark.Issue_Advance = model.Issue_Advance;
            ent.empSalaries.Add(mark);
            ent.SaveChanges();
            TempData["msg"] = "Advance has been Issued";
            return RedirectToAction("ViewProfile", new { id = model.EmpId });
        }

        //        public ActionResult totalAttendence()
        //        {
        //            var model = new Attendence();
        //            var query = @"SELECT 
        //	Attendence.EmpId, Employee.Name, 
        //	SUM(CASE WHEN [Date] IS NOT NULL THEN 1 ELSE 0 END) [Present],
        //	SUM(CASE WHEN [Date] IS NULL THEN 1 ELSE 0 END) [Absent],
        //	SUM(CASE WHEN [Date] IS NULL Then 1 else 0 end) [Leave]
        //FROM Attendence join Employee on Attendence.EmpId = Employee.EmpId 
        //GROUP BY Attendence.EmpId, Employee.Name ";
        //            var data = ent.Database.SqlQuery<ShowAttendence>(query).ToList();
        //            model. = data;
        //            return View(model);
        //        }

        public ActionResult employee(nclVM model, string OrgId, string DeptId)
        {

            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            string query = @"select emp.EmpId,emp.Name, NCL.dated, org.OrgId, org.OrgName,dep.DeptId, dep.DeptName, NCL.NCL_Balance from  Employee emp  JOIN Organisation org on org.OrgId = emp.OrgId join Department dep on dep.DeptId = emp.DeptId join NCL on NCL.Emp_Id = emp.EmpId where Datepart(mm,NCL.dated)= Month(GetDate()) and NCL.NCL_Balance IS NOT NULL group by emp.EmpId,emp.Name, NCL.dated, org.OrgId, org.OrgName,dep.DeptId, dep.DeptName, NCL.NCL_Balance";
            var data = ent.Database.SqlQuery<NCLrm>(query).ToList();
            if (string.IsNullOrEmpty(OrgId) && string.IsNullOrEmpty(DeptId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(model);

            }
            else
            {

                if (model != null && model.OrgId != null && model.DeptId != null)
                    data = data.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList();
                model.list = data;
                return View(model);
            }
        }

        public ActionResult SaveNCLs(nclVM model)
        {
            var OrgId = model.list.FirstOrDefault().OrgId;
            var DeptId = model.list.FirstOrDefault().DeptId;
            string msg = "";
            try
            {
                foreach(var item in model.list)
                {
                    var ncl = new NCL
                    {
                        NCL1 = item.NCL,
                        Emp_Id = item.EmpId,
                        dated = item.dated,
                    };
                    ent.NCLs.Add(ncl);
                }
                ent.SaveChanges();
                msg = "Successfully Saved!!";
            }
            catch (Exception ex)
            {
                msg = "Error!!";
            }
            TempData["msg"] = msg;
            return RedirectToAction("employee", new {OrgId, DeptId});
        }

        public ActionResult NCL(int EmpId)
        {
            var model = new nclVM();
            var query = @"select Emp_Id,IsNull(NCL,0) as NCL,dated, IsNull(NCL_Adjusted,0) as NCL_Adjusted , IsnULL(NCL_Balance,0) as NCL_Balance , Adjusted_Date, Remarks from NCL where Emp_Id=" + EmpId;
            var data = ent.Database.SqlQuery<NCLrm>(query).ToList();
            model.list = data;
            return View(model);

        }


        [HttpPost]
        public ActionResult NCL(nclVM model)
        {
            var mark = Mapper.Map<NCL>(model);
            //mark.dated = DateTime.Now;
            mark.Emp_Id = model.EmpId;
            mark.NCL1 = model.NCL1;
            ent.NCLs.Add(mark);
            ent.SaveChanges();
            TempData["msg"] = "NCL has been Allotted";
            return RedirectToAction("ViewProfile", new { id = model.EmpId });
        }


        public ActionResult GenerateNCL(int EmpId)

        {
            var model = new nclVM();
            var query = @"select emp.EmpId, emp.Name, leaves.Adjusted_Date as Adjusted_Date, IsNull(leaves.NCL_Balance,0) as Balance_NCL, leaves.Remarks from Employee emp join NCL leaves on emp.EmpId= leaves.Emp_Id WHERE DATEPART(m, Adjusted_Date) = DATEPART(m, DATEADD(m, -1, getdate())) AND DATEPART(yyyy, Adjusted_Date) = DATEPART(yyyy, DATEADD(m, -1, getdate())) and Emp_Id=" + EmpId;
            var data = ent.Database.SqlQuery<NCLrm>(query).ToList();
            model.list = data;
            return View(model);
        }

        [HttpPost]
        public ActionResult GenerateNCL(nclVM model)
        {
            var data = new NCL();
            //data.Dated = DateTime.Now;
            //data.Casual_Leave = 0;
            data.dated = model.dated;
            data.Emp_Id = model.EmpId;
            data.NCL1 = model.NCL1; //Get the Issued CLs of Month
            data.Remarks = model.Remarks;//Get the Remarks
            ent.NCLs.Add(data);
            ent.SaveChanges();
            TempData["msg"] = "NCL has been Generated";
            return RedirectToAction("ViewProfile", new { id = model.EmpId });
        }



        //public ActionResult changeProfile(int EmpId, string profilePic, HttpPostedFileBase profilePicName)
        //{
        //    try
        //    {
        //        var data = ent.Employees.Find(EmpId);
        //        if (EmpId > 0 && profilePicName != null)
        //        {
        //            var picName = FileOperation.UploadImage(profilePicName, "EmployeeImages");
        //            if (picName != "not allowed")
        //            {
        //                data.profilePic = profilePic;
        //                ent.SaveChanges();
        //            }
        //        }
        //        return RedirectToAction("ViewProfile");
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //    return RedirectToAction("ViewProfile");
        //}

        [HttpPost]
        public ActionResult ChnageProfile(HttpPostedFileBase Profileimage, int id)
        {
            string msg = "";
            try
            {
                var record = ent.Employees.Find(id);
                var picName = FileOperation.UploadImage(Profileimage, "EmployeeImages");
                if (picName != null)
                {
                    record.profilePic = picName;
                    ent.SaveChanges();
                }
                TempData["msg"] = "Profile Picture Changed.";
                return RedirectToAction("ViewProfile", new { id = record.EmpId });
            }

            catch (Exception ex)
            {
                msg = "error";
                return View();
            }

            // return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LogOutAll()
        {
            try
            {
                var query = @"Update Login set IsLogin = case when IsLogin=1 then 0 else 0 end where RoleId = 3 ";
                ent.Database.ExecuteSqlCommand(query);
                TempData["msg"] = "Successfully LogOut!!";
                return RedirectToAction("LogOut");
            }
            catch (Exception ex)
            {

            }
            return Content("An Error Has Been Occurred");
        }


        public ActionResult LogOut()
        {
            var model = new UserDTO();
            var query = @"select Login.Username,Login.Name, Login.Logintime, Login.IsLogin, Login.StatusId, role.RoleName as role from Login join Roles role on Login.RoleId = role.Id where [role].RoleName = 'User'";
            var data = ent.Database.SqlQuery<UsersVM>(query).ToList();
            model.UsersList = data;
            return View(model);
        }

        public ActionResult EditOption(string OrgId, string DepId)
        {
            var model = new AtendenceDTO();
            model.DepList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrgList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            string query = @"select att.Id, att.Attendence, att.Weightage, dep.DeptId as DepId, dep.DeptName as DepName,org.OrgId, org.OrgName from Atendence att join Department dep on att.DepId = dep.DeptId join Organisation org on att.OrgId = org.OrgId where org.OrgId='"+OrgId+"' and dep.DeptId='"+DepId+"'";
            var data = ent.Database.SqlQuery<AttendenceVMs>(query).ToList();
            if (string.IsNullOrEmpty(OrgId) && string.IsNullOrEmpty(DepId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(model);
            }
            else
            {
                model.attList = data;
                return View(model);
            }
        }





        public JsonResult SaveData(Atendence model)
        {
            string msg = "";
            try
            {
                //var attendanceMaster = ent.Atendences.Find(model.att_Id);
                var record = ent.Atendences.FirstOrDefault(a => a.Attendence == model.Attendence && a.Weightage == model.Weightage && a.OrgId == model.OrgId && a.DepId == model.DepId);
                if (record == null)
                {
                    ent.Atendences.Add(model);
                }
                else
                {
                    record.Attendence = model.Attendence;
                    record.Weightage = model.Weightage;
                    record.OrgId = model.OrgId;
                    record.DepId = model.DepId;

                }
                ent.SaveChanges();
                msg = "ok";
            }
            catch (Exception ex)
            {
                msg = "error";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);


        }


        [HttpPost]
        public ActionResult Finalise()
        {
            var model = new EmployeeAtt();
            var Month1 = TempData["month"];
            var OrgId = TempData["OrgId"];
            var DeptId = TempData["DeptId"];
            try
            {
                var query = @"Update Attendence set IsFinalised = 1";
                ent.Database.ExecuteSqlCommand(query);
                TempData["msg"] = "Attendence is Finalised";
                return RedirectToAction("ViewAttendece", new { Month1, OrgId, DeptId });
            }
            catch (Exception ex)
            {

            }
            return Content("An Error Has Been Occurred");
        }


        public ActionResult UpdateAttendence(EditAttendence model, string term, string OrganisationId, string DepartmentId)
        {
            model.AttendenceList = new SelectList(ent.Atendences.ToList(), "Id", "Attendence");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            string sqlQuery = "select att.Id as Id,EmpId as Emp_Id, Name,DeptId, OrgId from employee join Attendence att on Employee.EmpId = att.Emp_Id";
            var emp = ent.Database.SqlQuery<Update>(sqlQuery).ToList();
            if (string.IsNullOrEmpty(OrganisationId) && string.IsNullOrEmpty(DepartmentId))
            {
                ViewBag.Msg = "No Data is Present";
                return View(model);

            }
            else
            {

                if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                    emp = emp.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
                if (!string.IsNullOrEmpty(term))
                {
                    term = term.ToLower();
                    emp = emp.Where(a => a.Name.ToLower().Contains(term)).ToList();
                }

                model.List = emp;
                model.date = DateTime.Now.Date;
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult UpdateAtendence(EditAttendence data)
        {
            string msg = "";
            try
            {
                var record = ent.Attendences.Find(data.List[0].Id);
                if (record == null)
                {
                    return View(data);
                }
                else
                {
                    //ent.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    record.att_Id = data.List[0].Att_ID;
                    record.Date = data.date;
                    ent.SaveChanges();
                    return View("UpdateAttendence","Admin");
                }

            }
            catch (Exception ex)
            {
                msg = "error";
            }
            return View();
        }


        public ActionResult ShowAttendence(EditAttendence model)
        {
            var date = TempData["Date"];
            string sqlQuery = "select emp.Name, att.att_Id as Att_Id, Attendences.Attendence as AttendenceName from employee emp join Attendence att on emp.EmpId = att.Emp_Id join Atendence Attendences on Attendences.Id = att.att_Id where Date='" + date+"'";
            var emp = ent.Database.SqlQuery<EmployeeList>(sqlQuery).ToList();
            model.empList = emp;
            model.date = DateTime.Now.Date;
            return View(model);
        }
        }
    }







