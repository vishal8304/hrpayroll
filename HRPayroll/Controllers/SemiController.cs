﻿using AutoMapper;
using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Controllers
{
    [Authorize(Roles = "SemiAdmin")]
    public class SemiController : Controller
    {
        DBEntities ent = new DBEntities();

        public ActionResult semi()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CreateEmployee()
        {
            var model = new EmployeeDTO();
            //model.DepartmentId = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            return View(model);
        }
        [HttpPost]
        public ActionResult CreateEmployee(EmployeeDTO model)

        {
            try
            {
                if (!ModelState.IsValid)
                {
                    //model.Department = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
                    model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
                    return View(model);
                }
                var emp = Mapper.Map<Employee>(model);
                emp.IsDeleted = false;
                ent.Employees.Add(emp);
                ent.SaveChanges();
                TempData["msg"] = "Successfully Added";
                return RedirectToAction("CreateEmployee");
            }
            catch(Exception ex) {
                TempData["msg"] = "Server Error";
                return RedirectToAction("CreateEmployee");
            }
}

        public ActionResult EditAttendence(EditAttendence model)
        {
            model.AttendenceList = new SelectList(ent.Atendences.ToList(), "Id", "Attendence");
            string sqlQuery = "select EmpId as Id,Name,DeptId, OrgId from employee";
            var emp = ent.Database.SqlQuery<EmployeeList>(sqlQuery).ToList();
            if (model != null && model.OrganisationId != null && model.DepartmentId != null)
                emp = emp.Where(a => a.OrgId == model.OrganisationId && a.DeptId == model.DepartmentId).ToList();
            model.empList = emp;
            model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            model.DepartmentList = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            //Filter For Attendence 
            return View(model);
        }

        public ActionResult SaveAttendance(Attendence model)
        {
            string msg = "";
            try
            {
                //var attendanceMaster = ent.Atendences.Find(model.att_Id);
                var record = ent.Attendences.FirstOrDefault(a => a.Emp_Id == model.Emp_Id && a.Date == model.Date);
                if (record == null)
                {
                    ent.Attendences.Add(model);
                }
                else
                {

                    record.att_Id = model.att_Id;
                }
                ent.SaveChanges();
                msg = "ok";
            }
            catch (Exception ex)
            {
                msg = "error";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CasualLeave(LeavesRetrunModel model, int pageNumber = 1)
        {
            model.Departments = new SelectList(ent.Departments.ToList(), "DeptId", "DeptName");
            model.Organisation = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            DateTime today = DateTime.Today;
            var data = (from e in ent.Employees
                        join leave in ent.LeavesLedgers
                        on e.EmpId equals leave.Emp_Id into g
                        select new empLeave
                        {

                            Emp_Id = e.EmpId,
                            Name = e.Name,
                            Phone = e.Phone,
                            Address = e.Address,
                            Email = e.Email,
                            Gender = e.Gender,
                            Emergency_C = e.Emergency_C,
                            Casual_Leave = g.Sum(leave => leave.Casual_Leave),

                            availCL = g.Sum(leave => leave.availCL),
                            //salId=s.Id
                        }).ToList();
            int totalRecord = data.Count;
            int pageSize = 5;
            double totalPages = Math.Ceiling((double)totalRecord / pageSize);
            data = data.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            model.PageNumber = pageNumber;
            model.TotalPages = Convert.ToInt32(totalPages);
            if (model != null && model.OrgId != null && model.DeptId != null)
                data = data.Where(a => a.OrgId == model.OrgId && a.DeptId == model.DeptId).ToList();
            model.empList = data;
            return View(model);
        }
   

        public ActionResult MarkCl(int id)
        {
            var model = new LeavesRetrunModel();
            var data = (from e in ent.Employees
                        join leaves in ent.LeavesLedgers on e.EmpId equals leaves.Emp_Id into g
                        where e.EmpId == id
                        select new empLeave
                        {
                            Emp_Id = e.EmpId,
                            Name = e.Name,
                            availCL = g.Sum(leaves => leaves.availCL),
                            Casual_Leave = g.Sum(leaves => leaves.Casual_Leave)
                        }).ToList();
            model.empList = data;
            return View(model);
        }


        [HttpPost]
        public ActionResult MarkCl(LeavesRetrunModel model)
        {
            var mark = Mapper.Map<LeavesLedger>(model);
            mark.Dated = DateTime.Now;
            mark.IsDeleted = false;
            mark.Emp_Id = model.Emp_Id;
            ent.LeavesLedgers.Add(mark);
            ent.SaveChanges();
            return RedirectToAction("CasualLeave", new { id = model.Emp_Id });

        }


        public ActionResult IssueAdvance(string term)
        {

            var model = new IssueAdvance();
            //model.OrganizationList = new SelectList(ent.Organisations.ToList(), "OrgId", "OrgName");
            var data = (from emp in ent.Employees
                        join empSal in ent.empSalaries on emp.EmpId equals empSal.Emp_Id into g
                        select new AdvanceIssue
                        {
                            Emp_Id = emp.EmpId,
                            Name = emp.Name,
                            Issue_Advance = g.Sum(empSal => empSal.Issue_Advance),
                            Advance_Due = g.Sum(empSal => empSal.Advance_Due)
                        }).ToList();
            //if (model != null && model.OrganisationId != null && model.DepartmentId != null)
            //    data = data.Where(a => a.OrganisationId == model.OrganisationId && a.DepartmentId == model.DepartmentId).ToList();
            if (!string.IsNullOrEmpty(term))
            {
                data = data.Where(a => a.Name.StartsWith(term)).ToList();
            }
            model.IssueAdv = data;
            return View(model);
        }


        //Mark Advance of an Employee
        public ActionResult markAdvance(int Empid)
        {
            var model = new IssueAdvance();
            var data = (from emp in ent.Employees
                        join empSal in ent.empSalaries on emp.EmpId equals empSal.Emp_Id into e
                        where emp.EmpId == Empid
                        select new AdvanceIssue
                        {
                            Emp_Id = emp.EmpId,
                            Name = emp.Name,
                            Issue_Advance = e.Sum(empSal => empSal.Issue_Advance),
                            Advance_Due = e.Sum(empSal => empSal.Advance_Due)
                        }).ToList();
            model.IssueAdv = data;
            return View(model);


        }


        [HttpPost]
        public ActionResult markAdvance(IssueAdvance model)
        {
            var mark = Mapper.Map<empSalary>(model);
            mark.Dated = DateTime.Now;
            mark.IsDeleted = false;
            mark.Emp_Id = model.Emp_Id;
            ent.empSalaries.Add(mark);
            ent.SaveChanges();
            return RedirectToAction("IssueAdvance", new { Empid = model.Emp_Id });
        }

        public ActionResult GetDepartmentsByOrgId(int OrgnisationId)
        {
            var dpt = ent.Departments.Where(a => a.OrgId == OrgnisationId).ToList();
            var data = Mapper.Map<IEnumerable<DepartmentDTO>>(dpt);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewAttendece()
        {
            int Month = DateTime.Now.Month;

            // Create List
            List<EmployeeAtt> empWithDate = new List<EmployeeAtt>();

            using (var ent = new DBEntities())
            {
                empWithDate = ent.Attendences.Where(a => a.Date.Value.Month == Month)
                     .Select(a =>
                     new EmployeeAtt
                     {
                         EmpId = a.Id,
                         Date = a.Date,
                         Attendence = a.Atendence.Attendence
                     }).OrderBy(a => a.Date).ToList();

            }

            return View(empWithDate);
        }

        public ActionResult ViewAdvance(IssueAdvance model)
        {
            string query = @"select  emp.Name, Sum(empSal.Issue_Advance) as TotalIssueAdvance, Sum(empSal.Advance_Due) as TotalAdvanceDue from empSalary empSal join Employee emp on empSal.EmpId = emp.EmpId group by EMP.Name";
            var data = ent.Database.SqlQuery<AdvanceIssue>(query).ToList();
            model.IssueAdv = data;
            return ViewAdvance(model);
        }

    }
}