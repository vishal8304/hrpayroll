﻿using AutoMapper;
using HRPayroll.Models.DomainModels;
using HRPayroll.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.StartupTask
{
    public class AutomapperReg
    {
        public static void Register()
        {
            Mapper.CreateMap<Employee, EmployeeList>();
            Mapper.CreateMap<EmployeeList, Employee>();
            Mapper.CreateMap<Organisation, OrganisationDTO>();
            Mapper.CreateMap<OrganisationDTO, Organisation>();
            Mapper.CreateMap<Department, DepartmentDTO>();
            Mapper.CreateMap<DepartmentDTO, Department>();
            Mapper.CreateMap<SalaryLedger, ChangeSalaryDTO>();
            Mapper.CreateMap<ChangeSalaryDTO, SalaryLedger>();
            Mapper.CreateMap<SalaryLedger, SalaryLedgerDTO>();
            Mapper.CreateMap<SalaryLedgerDTO, SalaryLedger>();
            Mapper.CreateMap<UserDTO, Login>();
            Mapper.CreateMap<Login, UserDTO>();
            Mapper.CreateMap<LeavesLedgerDTO, LeavesLedger>();
            Mapper.CreateMap<LeavesLedger, LeavesLedgerDTO>();
            Mapper.CreateMap<LeavesRetrunModel, LeavesLedger>();
            Mapper.CreateMap<LeavesLedger, LeavesRetrunModel>();
            Mapper.CreateMap<empSalary, IssueAdvance>();
            Mapper.CreateMap<IssueAdvance, empSalary>();
            Mapper.CreateMap<empSalary, Advance>();
            Mapper.CreateMap<Advance, empSalary>();
            Mapper.CreateMap<Employee, EmployeeDTO>();
            Mapper.CreateMap<EmployeeDTO, Employee>();
            Mapper.CreateMap<NCL, NCLrm>();
            Mapper.CreateMap<NCLrm, NCL>();
            Mapper.CreateMap<NCL, nclVM>();
            Mapper.CreateMap<nclVM, NCL>();
            Mapper.CreateMap<Employee, EmployeeProfileModel>();
            Mapper.CreateMap<EmployeeProfileModel, Employee>();
            Mapper.CreateMap<SalaryMasters, SalaryMaster>();
            Mapper.CreateMap<SalaryMaster, SalaryMasters>();
            Mapper.CreateMap<SalaryMaster, SalaryMasterPost>();
            Mapper.CreateMap<SalaryMasterPost, SalaryMaster>();
            Mapper.CreateMap<ViewList, Attendence>();
            Mapper.CreateMap<Attendence, ViewList>();
            
        }
    }
}