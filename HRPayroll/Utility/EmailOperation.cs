﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace HRPayroll.Utility
{
    public class EmailOperation
    {
        public static bool SendEmail(string recipeint, string subject, string msg, bool IsBodyHtml)
        {
            try
            {
                string sender = ConfigurationManager.AppSettings["smtpUser"];
                string password = ConfigurationManager.AppSettings["smtpPass"];
                MailMessage message = new MailMessage();
                message.From = new MailAddress(sender);
                message.To.Add(recipeint);
                message.Subject = subject;
                message.Body = msg;
                message.IsBodyHtml = IsBodyHtml;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(sender, password);
                client.EnableSsl = true;
                client.Send(message);
                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}