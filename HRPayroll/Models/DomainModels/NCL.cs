//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRPayroll.Models.DomainModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class NCL
    {
        public int Id { get; set; }
        public Nullable<int> Emp_Id { get; set; }
        public Nullable<int> NCL1 { get; set; }
        public Nullable<System.DateTime> dated { get; set; }
        public Nullable<int> NCL_Balance { get; set; }
        public Nullable<int> NCL_Adjusted { get; set; }
        public Nullable<System.DateTime> Adjusted_Date { get; set; }
        public string Remarks { get; set; }
    
        public virtual Employee Employee { get; set; }
    }
}
