//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRPayroll.Models.DomainModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalaryLedger
    {
        public int Id { get; set; }
        public Nullable<decimal> CurrentSalary { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> leaveId { get; set; }
        public Nullable<int> empSalId { get; set; }
        public Nullable<int> EmpId { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual empSalary empSalary { get; set; }
        public virtual LeavesLedger LeavesLedger { get; set; }
    }
}
