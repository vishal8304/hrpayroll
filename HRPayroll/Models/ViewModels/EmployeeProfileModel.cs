﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class EmployeeProfileModel
    {
        public HttpPostedFileBase Profileimage { get; set; }
        public string profilePic { get; set; }
        public Employee Employee { get; set; }
        public IEnumerable<SalaryLedger> SalaryLedger { get; set; }
         
        public IEnumerable<NCL> nclList { get; set; }
        public empstatu empStatus { get; set; }

        public IEnumerable<empstatu> statuswa { get; set; }
        public IEnumerable<StatusMaster> Staus { get; set; }
        //public StatusMaster status { get; set; } 
        public int TotalPages { get; set; }
         public string Status { get; set; } 

        public int? StatusId { get; set; }
        public int PageNumber { get; set; }

        public IEnumerable<LeavesLedger> leaveslist { get; set; }

        public IList<empLeave> empList { get; set; }

        public IList<NCLrm> list { get; set; }

        public IList<AdvanceIssue> IssueAdv { get; set; }

        public IEnumerable<empSalary> empSalaryList { get; set; }

        public List<advanceLedgerRM> advanceList { get; set; }

        public double CurrentSalary { get; set; }

        public double advance { get; set; }

        public SelectList StatusList { get; set; }

        public int? Emp_Id { get; set; }

    }
}