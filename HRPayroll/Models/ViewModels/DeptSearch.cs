﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class DeptSearch
    {
        public string status { get; set;}
        public int EmpId { get; set; }
        public string name { get; set; }
    }
}