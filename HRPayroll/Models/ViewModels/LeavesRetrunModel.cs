﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class LeavesRetrunModel
    {
        public IEnumerable<LeavesLedger> leavesList { get; set; }

        public IList<empLeave> empList { get; set; }

        public int PageNumber { get; set; }

        public int TotalPages { get; set; }
        public string Name { get; set; }


        public SelectList Organisation { get; set; }

        public SelectList Departments { get; set; }

        public int? OrgId { get; set; }

        public int? DeptId { get; set; }
        [DefaultValue(1)]
        public int? availCL { get; set; }

        public int? CasualLeaves { get; set; }
        
        public int Emp_Id { get; set; }
        public int salId { get; set; }

        public SelectList OrganizationList { get; set; }
        public int? OrganisationId { get; set; }

        public int? DepartmentId { get; set; }

        public string Remarks { get; set; }

        public int EmpId { get; set; }
        public int Id { get; set; }

        public DateTime Adjusted_Date { get; set; }

        public Nullable<int> Balance_CL { get; set; }
        
        public DateTime? Dated { get; set; }

    }
}