﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class AttendenceDTO
    {
        public int? OrgId { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList OrgList { get; set; }

        public SelectList DeptList { get; set; }
        public int? DeptId { get; set; }
        public SelectList DepartmentsList { get; set; }

        public IEnumerable<EmployeeAtt> empAtt { get; set; }
        public IEnumerable<TP> T_P { get; set; }

        //public int T_P { get; set; }
    }

    public class AttendenceVM {
        public int T_P { get; set; }
        public int EmpId { get; set; }
        public string Name { get; set; }
        public int OrgId { get; set; }
        public string OrgName { get; set; }
        public int DeptId { get; set; }
        public string Attendence { get; set; }


    }
}