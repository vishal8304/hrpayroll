﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class generateSalaryRM
    {
        public List<SalaryEmployee> empSalary { get; set; }
        public IEnumerable<empSalList> empSalList { get; set; }

        public Double? CurrentMonthSalary { get; set; }

        public Double? AdvanceGiven { get; set; }

        public int? availCl { get; set; }

        public int? EncashLeaves { get; set; }

        public int EmpID { get; set; }

        public string Name { get; set; }

        public IEnumerable<IssueAdvance> empSal { get; set; }

        public String OrgName { get; set; }

        public String DeptName { get; set; }

       
        public Double? TotalSalary { get; set; }
        public int? DeptId { get; set; }
        public int? OrgId { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public int Leaves { get; set; }
    }

    public class SalaryEmployee
    {
        public double? Curr_Salary { get; set; }

        public double? AdvanceGiven { get; set; }

        public int? availCl { get; set; }

        public int? EncashLeaves { get; set; }

        public int EmpID { get; set; }

        public string Name { get; set; }

        public String OrgName { get; set; }

        public String DeptName { get; set; }

        public int? DeparmenttId { get; set; }
        public int? OrganisationId { get; set; }

        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }

        public int Leaves { get; set; }
    }
}