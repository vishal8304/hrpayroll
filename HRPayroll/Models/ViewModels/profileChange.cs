﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class profileChange
    {
        public int Id { get; set; }
        public int? RoleId {get;set;}

        public string RoleName { get; set; }
        public string Name { get; set; }

        public string UserName { get; set; }

        public SelectList Role { get; set; }
    }
}