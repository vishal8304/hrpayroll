﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class ShowAttendence
    {
        public int TP { get; set; }
        public int TA { get; set; }
    }
}