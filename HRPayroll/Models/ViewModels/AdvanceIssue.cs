﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class AdvanceIssue
    {
        public int Emp_Id { get; set; }//Foreign key
        public string Name { get; set; }
        public double? Issue_Advance { get; set; }
        public decimal? Advance_Due { get; set; }

        public int OrganisationId { get; set; }

        public int DepartmentId { get; set; }

        public DateTime? Dated { get; set; }

        public int OrgId { get; set; }
        public int DeptId { get; set; }
        public string OrgName { get; set; }

        public string DeptName { get; set; }
        public decimal? Balance_Advance { get; set; }

        public DateTime? Adjusted_Date { get; set; }
        public string Remarks { get; set; }
    }
}