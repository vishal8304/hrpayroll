﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HRPayroll.Models.ViewModels
{
    public class OrganisationDTO
    {
        public int OrgId { get; set; }
        [Required(ErrorMessage ="Organization name is required." )]
        public string OrgName { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}