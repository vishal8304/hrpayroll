﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class LeaveAmount
    {
        public int? FL { get; set; }


        public int? CL { get; set; }

        public int? NCL { get; set; }
        public int? TL { get; set; }

        public decimal? currentSalary { get; set; }

        public IEnumerable<empSalary> empSalList { get; set; }

        public IEnumerable<LeavesLedger> empLeavesList { get; set; }

        public IEnumerable<Attendence> empAttList { get; set; }

        public IEnumerable<SalaryLedger> empsalaryList { get; set; }

        public IEnumerable<NCL> empNCLList { get; set; }

        public decimal? amt { get; set; }

        public decimal? amt1 { get; set; }


        public int? EncashedCls { get; set; }
    }
}