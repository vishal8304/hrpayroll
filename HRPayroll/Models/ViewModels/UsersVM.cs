﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class UsersVM
    {

        public int Id { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string Username { get; set; }
        public string Passwrd { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Logintime { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        public int? StatusId { get; set; }
        public string role { get; set; }
        public bool IsLogin { get; set; }


    }
}