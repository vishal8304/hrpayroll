﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class advanceLedgerRM
    {

        public int eMPSalId { get; set; }
        public Nullable<int> EmpId { get; set; }
        public Nullable<double> Issue_Advance { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<double> Advance_Due { get; set; }
        public Nullable<System.DateTime> Dated { get; set; }
        public int sALLedgerId { get; set; }
        public Nullable<int> EmpSalId { get; set; }
        public double CurrentSalary { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Remarks { get; set; }
    }
}