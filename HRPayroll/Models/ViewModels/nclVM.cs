﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class nclVM
    {

        public int EmpId { get; set; }
        public Nullable<int> Emp_Id { get; set; }
        public Nullable<int> NCL1 { get; set; }
        public Nullable<System.DateTime> dated { get; set; }
        public Nullable<int> NCL_Adjusted { get; set; }
        public Nullable<int> NCL_Balance { get; set; }
        public IList<NCLrm> list { get; set; }
        public string Remarks { get; set; }
        public SelectList DepartmentList { get; set; }
        public SelectList OrganizationList { get; set; }
        public int OrgId { get; set; }
        public int DeptId { get; set; }
        public string DeptName { get; set; }
        public string OrgName { get; set; }
        public string Name { get; set; }
        public DateTime? Adjusted_Date { get; set; }
        public  NCLrm NCLS { get; set; }
    }
}