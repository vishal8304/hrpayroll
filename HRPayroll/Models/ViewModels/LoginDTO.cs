﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class LoginDTO

    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Username { get; set; }
        public string Passwrd { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Logintime { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        public IEnumerable<Login> list { get; set; }
        public string RoleName { get; set; }

        


    }

}