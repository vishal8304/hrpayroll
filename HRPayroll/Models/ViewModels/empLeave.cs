﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class empLeave
    {
      
        public int EmpId { get; set; }
        public string Name { get; set; }
        public Nullable<int> DeptId { get; set; }
        public Nullable<int> OrgId { get; set; }
        public DateTime DOB { get; set; }
        public DateTime Joining_Date { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public DateTime? Dated { get; set; }
        public string Phone { get; set; }
        public string Mother_Name { get; set; }
        public string Father_Name { get; set; }
        public string H_F_Name { get; set; }
        public string Email { get; set; }
        public string Emergency_C { get; set; }
        public string Aadhar_Number { get; set; }
        public string DeptName { get; set; }
        public string OrgName { get; set; }
        public string status { get; set; }
        public double Salry { get; set; }
         public int salId { get; set; }

        public double CurrentSalary { get; set; }

        public int Id { get; set; }
        public int Emp_Id { get; set; }
        public Nullable<int> Salary { get; set; }
        public int? Casual_Leave { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

        public DateTime? Adjusted_Date { get; set; }
        public int? availCL { get; set; }

        public int? Balance_CL { get; set; }

        public string Remarks { get; set; }

     
    }
}