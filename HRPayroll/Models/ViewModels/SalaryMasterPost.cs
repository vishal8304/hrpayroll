﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class SalaryMasterPost
    {
        public int? Id { get; set; }
        public int? Emp_Id { get; set; }
        public decimal? CurrentSalary { get; set; }
        public string Name { get; set; }
        public int? TP { get; set; }
        public int? TL { get; set; }
        public int? CL1 { get; set; }
        public int? OrgId { get; set; }
        public int? DeptId { get; set; }
        public int? ncl { get; set; }
        public int? total { get; set; } //Final Leaves
        public decimal? LeaveAmount { get; set; }
        public decimal? AdvanceAmount { get; set; }
        public decimal? adjustedAmt1 { get; set; } // Adjusted Amount
        //public decimal? AdvanceBalance { get; set; }
        public bool IsPaid { get; set; }
        public int? encashed { get; set; }  //Encashed CLs
        public decimal? amount { get; set; } //Encash Amount
        public decimal? salary { get; set; }

        public DateTime? Dated { get; set; }

        public string OrgName { get; set; }

        public string DeptName { get; set; }

        public string Status { get; set; }
    }
}