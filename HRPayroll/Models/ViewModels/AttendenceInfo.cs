﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class AttendenceInfo
    {
        public int EmpId { get; set; }
        public int Att_Id { get; set; }

        public DateTime Date { get; set; }

    }
}