﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class SalaryLedgerDTO
    {
        public int Id { get; set; }
        public Nullable<int> EmpId { get; set; }
        [Required(ErrorMessage = "Required to fill")]
        public decimal CurrentSalary { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}