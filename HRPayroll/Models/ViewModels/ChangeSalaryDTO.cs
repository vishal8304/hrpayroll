﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class ChangeSalaryDTO
    {
        public IEnumerable<Salary> salaryUpdate { get; set; }
        public System.DateTime Date { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public int? OrgId { get; set; }
        public int? DeptId { get; set; }
        public string Name { get; set; }
        public Nullable<int> EmpId { get; set; }
        public double? New_Salary { get; set; }
        public string Remarks { get; set; }

    }

    public class Salary
    {
        public int Sal_Id { get; set; }
        public Nullable<int> EmpId { get; set; }
        public double? New_Salary { get; set; }
        public string Remarks { get; set; }
        public string Name { get; set; }
        public double Curr_Salary { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public int? OrgId { get; set; }
        public int? DeptId { get; set; }
    }
    
}