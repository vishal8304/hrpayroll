﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class AtendenceDTO
    {
        public int Id { get; set; }
        public string Attendence { get; set; }
        public string Weightage { get; set; }
        public SelectList  OrgList { get; set; }

        public SelectList DepList { get; set; }
        public Nullable<int> OrgId { get; set; }
        public Nullable<int> DepId { get; set; }
        public Nullable<int> OrganisationId { get; set; }
        public Nullable<int> DeptId { get; set; }
        public IEnumerable<AttendenceVMs> attList { get; set; }

       

    }
}