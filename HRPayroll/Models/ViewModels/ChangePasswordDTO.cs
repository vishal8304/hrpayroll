﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class ChangePasswordDTO
    {
        [Required]
        public string password { get; set; }
        [Compare("password",ErrorMessage ="Password and confirm password does not match.")]
        public string Confirmpwd { get; set; }
        public int Id { get; set; }
    }
}