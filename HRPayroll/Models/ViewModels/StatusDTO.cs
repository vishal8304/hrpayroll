﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class StatusDTO
    {

        public List<Status> empList { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public int? OrganisationId { get; set; }
        public int? DepartmentId { get; set; }
        public SelectList StatusList { get; set; }

        public int TotalPages { get; set; }
        public int PageNumber { get; set; }
    }

    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DeptId { get; set; }
        public string DeptName { get; set; }
        public string OrgName { get; set; }
        public int OrgId { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DOJ { get; set; }
        public int? CL_Available { get; set; }
        public Double? AdvanceDue { get; set; }
        public string empstatus { get; set; }
        public int StatusId { get; set; }
        public int TotalPages { get; set; }
        public int PageNumber { get; set; }

    }
}