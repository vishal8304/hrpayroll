﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class salDetails
    {

        public int Id { get; set; }
        public Nullable<int> EmpId { get; set; }

        public double CurrentSalary { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}