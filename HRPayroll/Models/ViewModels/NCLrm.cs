﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class NCLrm
    {
        public int EmpId { get; set; }
        public Nullable<int> Emp_Id { get; set; }
        public Nullable<int> NCL { get; set; }
        public Nullable<int> NCL_Adjusted { get; set; }
        public Nullable<int> NCL_Balance { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> dated { get; set; }
        public string EmpName { get; set; }
        public string Remarks { get; set; }
        public DateTime? Adjusted_Date { get; set; }
        public int? OrgId { get; set; }
        public int? DeptId { get; set; }
        public string DeptName { get; set; }
        public string OrgName { get; set; }
    }
}