﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class NameOfEmployee
    {
        public string EmpName { get; set; }
        public int EmpId { get; set; }
    }
}