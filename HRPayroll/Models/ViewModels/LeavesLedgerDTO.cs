﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class LeavesLedgerDTO
    {
        public int? Id { get; set; }
        public Nullable<int> Emp_Id { get; set; }
        public Nullable<int> Salary { get; set; }
        public int? Casual_Leave { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> availCL { get; set; }
        public Nullable<System.DateTime> Dated { get; set; }
        public SelectList Organisation { get; set; }

        public SelectList Departments { get; set; }

        public int? OrgId { get; set; }

        public int? DeptId { get; set; }


        public IEnumerable<EmployeeDTO> empList { get; set; }

        public int PageNumber { get; set; }

        public int TotalPages { get; set; }

     
    }
}