﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class AttendenceVMs
    {
        public int Id { get; set; }
        public string Attendence { get; set; }
        public int? Weightage { get; set; }
        public Nullable<int> OrgId { get; set; }
        public Nullable<int> DepId { get; set; }

        public string OrgName { get; set; }

        public string DepName { get; set; }


    }
}