﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRPayroll.Models.ViewModels
{
    public class BasicDetails
    {
        public int? EmpId { get; set; }
        public  int a = 1;
        public string Name { get; set; }
        public string fls = "fls";
        public int? CL { get; set; }

        public int? EncashedLeaves { get; set; }

        public int? Serial_No { get; set; }

        public int? OrgId { get; set; }

        public int? DeptId { get; set; }

    }
}