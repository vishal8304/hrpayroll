﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class UserDTO
    {
        public int Id { get; set; }
        public Nullable<int> RoleId { get; set; }

        //public SelectList RoleId { get; set; }
        [Required(ErrorMessage ="User Name is Required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Password should be in Number only.")]
        public string Passwrd { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Logintime { get; set; }
        public string Name { get; set; }

      //[Required(ErrorMessage ="Kindly Check Status")]
        public string Status { get; set; }

        //[Required(ErrorMessage ="Kindly Check Role") ]
        public string role { get; set; }
      
        public SelectList RoleList { get; set; }
        public SelectList StatusList { get; set; }

        public int? statusId { get; set; }

        public IEnumerable<UsersVM> UsersList { get; set; }

        public IEnumerable<profileChange> UsersChange{ get; set; }

        public int TotalPages { get; set; }

        public int PageNumber { get;set; }

        public bool IsLogin { get; set; }
    }
}