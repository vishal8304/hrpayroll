﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class EmployeeDTO
    {
        public int EmpId { get; set; }
        [Required(ErrorMessage = "Select any Department ")]
        public Nullable<int> DeptId { get; set; }
        public SelectList DepartmentList { get; set; }
        public SelectList OrganizationList { get; set; }

        public Nullable<int> OrganisationId { get; set; }
        [Required(ErrorMessage = "Organization is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Date of Birth is Required")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Joining Date is Required")]
        public DateTime Joining_Date { get; set; }

        [Required(ErrorMessage = "Select any Gender")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Address is Required")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Contact Number is Required")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Mother Name is Required")]
        public string Mother_Name { get; set; }
        [Required(ErrorMessage = "Father Name is Required")]
        public string Father_Name { get; set; }
        [Required(ErrorMessage = "Husband or Wife Name is Required")]
        public string H_F_Name { get; set; }
        [Required(ErrorMessage = "Email is Required")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Emergency Contact Number is Required")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string Emergency_C { get; set; }

        [Required(ErrorMessage = "Aadhar Number is Required")]
        [RegularExpression(@"^\S*$", ErrorMessage = "No white space allowed")]
        public string Aadhar_Number { get; set; }

        public string DeptName { get; set; }
        public string OrgName { get; set; }
        [Required(ErrorMessage ="Kindly Check the Status of Employee")]
        public string status { get; set; }
        [Required(ErrorMessage ="Insert Initial Salary of Employee")]
        public decimal? Salry { get; set; }


        public IList<NameOfEmployee> empName { get; set; }
        public IEnumerable<empdetails> EmpListDetails { get; set; }

        public IEnumerable<salDetails> SalListDetails { get; set; }


        public IEnumerable<LeavesLedgerDTO> leavesList { get; set; }

        public HttpPostedFileBase Profileimage { get; set; }
      
        public string profilePic { get; set; }

        public string panCard { get; set; }
     

    }
}