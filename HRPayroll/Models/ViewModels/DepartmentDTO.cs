﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class DepartmentDTO
    {
        public int DeptId { get; set; }
        [Required]
        public string DeptName { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        [Required]
        public Nullable<int> OrgId { get; set; }
        public SelectList Organizations { get; set; }
    }
}