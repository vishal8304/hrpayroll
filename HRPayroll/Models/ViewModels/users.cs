﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class users
    {

        public int Id { get; set; }
        public string userStatus { get; set; }

        public string Name { get; set; }
        public string Status { get; set; }
        public SelectList StatusList { get; set; }
        public int? statusId { get; set; }
    }
}