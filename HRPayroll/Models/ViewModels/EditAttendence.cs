﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class EditAttendence
    {
        
        public List<EmployeeList> empList { get; set; }
        public List<Role> Role {get;set;}
        
        public IEnumerable<ViewList> view { get; set; }
        public int ListId { get; set; }
        public List<Update> List { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public string DepartmentName { get; set; }
        public int? OrganisationId { get; set; }
        public int? DepartmentId { get; set; }
        public SelectList AttendenceList { get; set; }
        public DateTime date { get; set; }
        public int Id { get; set; }
        public int? Emp_Id { get; set; }
        public int Att_ID { get; set; }
        public string AttName { get; set; }

        public IEnumerable<Attendence> Att { get; set; }
    }

    public class EmployeeList
    {
        public int AttendenceId { get; set; }
        public DateTime DOB { get; set; }
        public string Emergency_C { get; set; }
        public int ListId { get; set; }
        public string Gender { get; set; }
        public string H_F_Name { get; set; }
        public string Email { get; set; }
        public int EmpId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int DeptId { get; set; }
        public int OrgId { get; set; }
        public int? Emp_Id { get; set; }
        public string DeptName { get; set; }

        public string OrgName { get; set; }
        public string Phone { get; set; }

        public string Mother_Name { get; set; }

        public string Father_Name { get; set; }
        public string Address { get; set; }

        public string Aadhar_Number { get; set; }
        public int Att_ID { get; set; }

        public string AttendenceName { get; set; }
    }
        public class Update
        {

            public DateTime date { get; set; }
        
            public int? Emp_Id { get; set; }
            public int Att_ID { get; set; }
        public string Name { get; set; }
        public int DeptId { get; set; }
        public int OrgId { get; set; }
        public SelectList AttendenceList { get; set; }
        public int Id { get; set; }
    }

    public class ViewList
    {
        public string Name { get; set; }
        public string AttendenceName { get; set; }
        public int att_Id { get; set; }

    }
    }
