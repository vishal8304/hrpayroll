﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class SalaryMasters
    {
        public List<SalaryMasterPost> salaryMasterPost { get; set; }

        public int? OrgId { get; set; }

        public int? DeptId { get; set; }

        public SelectList OrgList { get; set; }

        public SelectList DeptList { get; set; }

    }
}