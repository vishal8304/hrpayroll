﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class EmployeeAtt
    {
        public int Attendence_Id { get; set; }
        public string Attendence { get; set; }
        public int? EmpId { get; set; }
        public string Emp_Name { get; set; }

        public string Name { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan inTime { get; set; }
        public TimeSpan outTime { get; set; }
        //public DateTime? date { get; set; }
     
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }

        public IEnumerable<Organisation> orgList { get; set; }

        public int? OrgId { get; set; }
        public  int? DeptId { get; set; }

        public IEnumerable<TP> t_P { get; set; }

        public int T_P { get; set; }

        public bool IsFinalised { get; set; }
    }

 }
