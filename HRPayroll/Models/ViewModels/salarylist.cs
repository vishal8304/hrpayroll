﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class salarylist
    {

        public SelectList OrgList { get; set; }
        public SelectList DeptList { get; set; }

        public int? OrgId { get; set; }

        public int? DeptId { get; set; }

        public DateTime? date { get; set; }

        public IEnumerable<BasicDetails> BasicDetails { get; set; }

 
       

    }
}