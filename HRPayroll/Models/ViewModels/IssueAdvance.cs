using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class IssueAdvance
    {

        public int Emp_Id { get; set; }//Foreign key
        public int EmpId { get; set; }
        public string Name { get; set; }
        public double? Issue_Advance { get; set; }
        public decimal? Advance_Due { get; set; }

        public int? OrganisationId { get; set; }

        public int? DepartmentId { get; set; }
         public SelectList OrganizationList { get; set; }


        public DateTime? Dated { get; set; }

        public IList<AdvanceIssue> IssueAdv { get; set; }
        public int OrgId { get; set; }
        public int DeptId { get; set; }
        public string OrgName { get; set; }

        public string DeptName { get; set; }
        public decimal Balance { get; set; }
        public DateTime? AdjustedDate { get; set; }

        public string Remarks { get; set; }
    }
}