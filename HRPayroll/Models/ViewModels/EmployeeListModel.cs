﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class EmployeeListModel
    {
        public IList<EmployeeList> emplist { get; set; }
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public int ? OrganisationId { get; set; }
        public int ? DepartmentId { get; set; }
        public SelectList StatusList { get; set; }
        public int? StatusId { get; set; }
        public int NumberOfPages { get; set; }
        public DateTime DOB { get; set; }

        public int TotalPages { get; set; }

        public int PageNumber { get; set; }


    }
}