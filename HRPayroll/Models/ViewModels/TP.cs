﻿using HRPayroll.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class TP
    {

        public int? T_P { get; set; }
      
        public int? T_L { get; set; }
        
    }
}