﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRPayroll.Models.ViewModels
{
    public class ViewSalary
    {
        public SelectList OrganizationList { get; set; }
        public SelectList DepartmentList { get; set; }
        public int? OrganisationId { get; set; }
        public int? DepartmentId { get; set; }
        public IEnumerable<salaryList> viewsalary {get; set;}
    }

    public class salaryList
    {
       public int Id {get; set;}
       public string Name {get; set;}
       public int? SalaryView { get; set;}  
        public double Curr_Salary { get; set; }
        public string DeptName { get; set; }
        public string OrgName { get; set; }
        public int? OrgId { get; set; }
        public int? DeptId { get; set; }
    }
}